import { isAuthenticated, unAuthenticate } from '@hooks/auth'
import { AwesomeError } from '@models/error'
import {
  Account,
  CompleteProfile,
  CreateInitialProfile,
  Profile,
} from '@models/profile'
import { CreateUser, User } from '@models/user'
import {
  apiConnection,
  decodeSanityPayload,
  encodeSanityPayload,
} from '@utils/sanity'
import { compare, genSalt, hash } from 'bcryptjs'
import { v4 } from 'uuid'

/**
 * # AccountService
 *
 * handle all of the account logic,
 * including auth, profile, and user
 */
class AccountService {
  deleteAccount: () => Promise<void> = async () => {
    try {
      const auth = await isAuthenticated()
      if (auth) {
        // delete the user and, profile
        await apiConnection.delete(auth.id)
        await apiConnection.delete({
          query: `*[_type == "profile" && reference($userId)][0]`,
          params: {
            userId: auth.id,
          },
        })
        await unAuthenticate()
      }
    } catch (error) {}
  }

  changePassword: (formData: any) => Promise<void> = async (formData) => {
    try {
      const currentPassword = formData.currentPassword
      const newPassword = formData.newPassword
      const confirmPassword = formData.confirmPassword
      const loadUserQuery = `*[_type == "user" && _id == $userId][0]`

      const auth = await isAuthenticated()

      if (auth) {
        const userResult = decodeSanityPayload<User>(
          await apiConnection.fetch(loadUserQuery, { userId: auth.id })
        )

        const isPasswordValid = await this.validatePassword({
          password: currentPassword,
          hashedPassword: userResult.password,
        })
        if (isPasswordValid) {
          if (newPassword == confirmPassword) {
            const hashedPassword = await this.hashPassword(newPassword)
            await apiConnection
              .patch(auth.id)
              .set({ password: hashedPassword })
              .commit()
          } else {
            throw {
              code: 'change-password/new-password-not-match',
              message:
                'Please ensure type the same password with the new password',
            } as AwesomeError
          }
        } else {
          throw {
            code: 'change-password/invalid-password',
            message:
              'Opps, the current password is not match. Please type the correct email',
          } as AwesomeError
        }
      }
    } catch (error) {
      if (typeof error == 'object') {
        throw error
      } else {
        throw {
          code: 'general',
          message: 'Opps, something error when try to change password',
        } as AwesomeError
      }
    }
  }

  completeProfile: (formData: any) => Promise<void> = async (formData) => {
    try {
      const input: CompleteProfile = {
        email: formData.email,
        id:
          formData.profileId != null && formData.profileId != ''
            ? formData.profileId
            : v4(),
        type: 'profile',
        username: formData.username,
        bio: formData.bio,
        birthDate: formData.birthDate,
        fullName: formData.fullName,
        location: formData.location,
      }

      // upload the avatar, if exists
      if (formData.avatar && formData.avatar.length > 0) {
        const file = formData.avatar[0] as File
        const refId = await (
          await apiConnection.assets.upload('image', file)
        )._id

        input.avatar = {
          type: 'image',
          asset: { type: 'reference', ref: refId },
        }
      }
      // upload the cover, if exists
      if (formData.cover && formData.cover.length > 0) {
        const file = formData.cover[0] as File
        const refId = await (
          await apiConnection.assets.upload('image', file)
        )._id

        input.cover = {
          type: 'image',
          asset: { type: 'reference', ref: refId },
        }
      }

      await apiConnection
        .patch(formData.profileId)
        .set({ ...encodeSanityPayload(input) })
        .commit()
    } catch (error) {
      throw {
        code: 'general',
        message: 'Something wrong when try to complete the profil',
      } as AwesomeError
    }
  }

  loadAccount: () => Promise<Account | undefined | null> = async () => {
    try {
      const auth = await isAuthenticated()
      if (auth) {
        const loadAccountQuery = `
        {
          "profile": *[_type == "profile" && user._ref == $userId][0]
            {
              ..., 
              "avatar": avatar.asset->url,
              "cover": cover.asset->url,
            },
          "user": *[_type == "user" && _id == $userId][0]
        }
      `

        const result = decodeSanityPayload<Account>(
          await apiConnection.fetch(loadAccountQuery, { userId: auth.id })
        )
        return result
      }
      return null
    } catch (error) {
      throw {
        code: 'general',
        message: 'Opps, something error when loading account',
      } as AwesomeError
    }
  }

  isProfileCompleted: (params: { userId: string }) => Promise<boolean> =
    async ({ userId }) => {
      try {
        const profileQuery = `*[_type == "profile" && user._ref == $userId][0]`

        const result = decodeSanityPayload<Profile>(
          await apiConnection.fetch(profileQuery, { userId })
        )
        if (result) {
          if (
            result.avatar &&
            result.username &&
            result.email &&
            result.fullName
          ) {
            return true
          }
        }
        return false
      } catch (error) {
        throw {
          code: 'general',
          message: 'Opps, something error when load the profile',
        } as AwesomeError
      }
    }

  validatePassword: (input: {
    password: string
    hashedPassword: string
  }) => Promise<boolean> = async ({ hashedPassword, password }) => {
    const isPasswordMatch = await compare(password, hashedPassword)
    return isPasswordMatch
  }

  hashPassword: (password: string) => Promise<string> = async (password) => {
    const saltRounds = 10
    const generatedSalt = await genSalt(saltRounds)
    const hashedPassword = await hash(password, generatedSalt)
    return hashedPassword
  }

  createInitialProfile: (params: { user: User }) => Promise<void> = async ({
    user,
  }) => {
    try {
      const input: CreateInitialProfile = {
        id: v4(),
        type: 'profile',
        username: user.identifier.substring(
          0,
          user.identifier.indexOf('@') - 1
        ),
        user: { ref: user.id, type: 'reference' },
      }

      await apiConnection.create(encodeSanityPayload(input))
    } catch (error) {
      throw {
        code: 'general',
        message: 'Opps, something wrong when try to add profile',
      } as AwesomeError
    }
  }

  signOut: () => Promise<void> = async () => {
    try {
      await unAuthenticate()
    } catch (error) {
      throw {
        code: 'general',
        message: 'Opps, something error when try to sign out',
      } as AwesomeError
    }
  }

  signIn: (formData: any) => Promise<User> = async (formData) => {
    try {
      const identifier = formData.identifier
      const password = formData.password

      const findUserQuery = `*[_type == "user" && identifier == $identifier][0]`
      const result = decodeSanityPayload<User>(
        await apiConnection.fetch(findUserQuery, { identifier })
      )
      if (result) {
        // check the current password
        const isPasswordValid = await this.validatePassword({
          hashedPassword: result.password,
          password,
        })
        if (isPasswordValid) {
          return result
        } else {
          throw {
            code: 'auth/password-incorrect',
            message: 'Opps, incorrect password. Please use another password',
          } as AwesomeError
        }
      } else {
        // create a new one user
        const hashedPassword = await this.hashPassword(password)
        const input: CreateUser = {
          identifier,
          password: hashedPassword,
          provider: ['password'],
          role: 'user',
          status: 'active',
          id: v4(),
          type: 'user',
        }
        const createdUserResult = decodeSanityPayload<User>(
          await apiConnection.create(encodeSanityPayload(input))
        )
        await this.createInitialProfile({ user: createdUserResult })
        return createdUserResult
      }
    } catch (error: any) {
      if (typeof error == 'object') {
        throw error
      } else {
        throw {
          code: 'general',
          message: 'Opps, something error when try to sign in',
        } as AwesomeError
      }
    }
  }
}

const accountService: AccountService = new AccountService()
export default accountService
