import emailJs from '@emailjs/browser'

/**
 * # ContactService
 *
 * handle the contact logic from the api
 * and calls
 */
class ContactService {
  sendMessage: (params: {
    fullName: string
    interests: string
    email: string
    message: string
  }) => Promise<void> = async ({ fullName, email, interests, message }) => {
    try {
      // define the template vars
      const templateVars = {
        name: fullName,
        email,
        interests,
        message,
      }

      await emailJs.send(
        process.env.NEXT_PUBLIC_EMAILJS_SERVICE_ID ?? '',
        process.env.NEXT_PUBLIC_EMAILJS_TEMPLATE_ID ?? '',
        templateVars,
        process.env.NEXT_PUBLIC_EMAILJS_PUBLIC_KEY ?? ''
      )
    } catch (error) {
      throw 'Opps, something error when try to send the message'
    }
  }
}

const contactService: ContactService = new ContactService()
export default contactService
