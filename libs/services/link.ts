import { isAuthenticated } from '@hooks/auth'
import { AwesomeError } from '@models/error'
import { CreateNewShortenLink, ShortenLink, SummaryLink } from '@models/link'
import {
  apiConnection,
  decodeSanityPayload,
  encodeSanityPayload,
} from '@utils/sanity'
import { v4 } from 'uuid'

/**
 * # LinkService
 *
 * the service class to handle the link data retrieve, mutation and another
 * logic
 */
class LinkService {
  deleteLink: (id: string) => Promise<void> = async (id) => {
    try {
      await apiConnection.delete(id)
    } catch (error) {
      throw {
        code: 'general',
        message: 'Opps, something error when delete the links',
      } as AwesomeError
    }
  }

  loadLinks: (
    page: number,
    order?: string,
    query?: string
  ) => Promise<{
    links: ShortenLink[]
    hasMore: boolean
  }> = async (page = 0, sortOrder, query = '') => {
    try {
      const auth = await isAuthenticated()

      // define the paginated
      const limit = 8
      const start = page * limit
      const end = (page + 1) * limit

      // define the orders
      let order = '_createdAt desc'
      if (sortOrder == 'Oldest') {
        order = '_createdAt asc'
      }
      if (sortOrder == 'Most Views') {
        order = 'totalView desc'
      }

      const loadLinksQuery = `*[_type == "link" && references($ownerId) && shortLink match "*${query}*"] | order(${order})[$start...$end]`
      const result = decodeSanityPayload<ShortenLink[]>(
        await apiConnection.fetch(loadLinksQuery, {
          ownerId: auth?.id,
          start,
          end,
          query,
        })
      )
      return {
        links: result,
        hasMore: result.length > 0 && result.length == limit,
      }
    } catch (error) {
      throw {
        code: 'general',
        message: 'Opps, something error when load the links',
      } as AwesomeError
    }
  }

  loadPopularLinks: () => Promise<ShortenLink[]> = async () => {
    try {
      const auth = await isAuthenticated()
      const loadPopularLinksQuery = `*[_type == "link" && references($ownerId)] | order(totalView desc)[0...4]`
      const result = decodeSanityPayload<ShortenLink[]>(
        await apiConnection.fetch(loadPopularLinksQuery, {
          ownerId: auth!.id,
        })
      )

      return result
    } catch (error) {
      throw {
        code: 'general',
        message: 'Opps, something error when try to load the sumary status',
      } as AwesomeError
    }
  }

  loadSummary: () => Promise<SummaryLink> = async () => {
    try {
      const auth = await isAuthenticated()
      const loadSummaryQuery = `
        {
          "totalLinks": count(*[_type == "link" && references($ownerId)]),
          "views": *[_type == "link" && references($ownerId)].totalView
        }
      `
      const result = decodeSanityPayload<any>(
        await apiConnection.fetch(loadSummaryQuery, {
          ownerId: auth!.id,
        })
      )

      const views = result.views as number[]
      const totalViews = views.reduce((total, view) => {
        total += view
        return total
      }, 0)
      return { totalLinks: result.totalLinks, totalViews }
    } catch (error) {
      throw {
        code: 'general',
        message: 'Opps, something error when try to load the sumary status',
      } as AwesomeError
    }
  }

  loadSourceUrl: (params: {
    shortLink: string
  }) => Promise<string | null | undefined> = async ({ shortLink }) => {
    try {
      const loadSourceUrlQuery = `*[_type == "link" && shortLink == $shortLink][0]`
      const result = decodeSanityPayload<ShortenLink>(
        await apiConnection.fetch(loadSourceUrlQuery, { shortLink })
      )
      if (result) {
        await apiConnection.patch(result.id).inc({ totalView: 1 }).commit()
        return result.sourceLink
      }
      return null
    } catch (error) {
      throw {
        code: 'general',
        message: 'Opps, something error when loading the shorten link',
      } as AwesomeError
    }
  }

  createRandomShortLink: (params: {
    customLink?: string
    length?: number
  }) => Promise<string> = async ({ customLink, length = 10 }) => {
    try {
      let generatedShortenLink = customLink
      let isUnique = false

      const randomLink = () =>
        Math.random()
          .toString(36)
          .substring(2, length)
          .split('')
          .map((e) => (Math.random() < Math.random() ? e.toUpperCase() : e))
          .join()
          .replaceAll(',', '')

      // now let's check the unique
      // of the generated shorten link
      do {
        if (!customLink && customLink == '') {
          generatedShortenLink = randomLink()
        }

        const checkUniqueLinkQuery = `*[_type == "link" && shortLink == $generatedShortenLink][0]`
        const checkUniqueLinkResult = decodeSanityPayload<any>(
          await apiConnection.fetch(checkUniqueLinkQuery, {
            generatedShortenLink,
          })
        )
        if (checkUniqueLinkResult) {
          isUnique = false
        } else {
          isUnique = true
        }
      } while (!isUnique)
      {
        if (!customLink && customLink == '') {
          generatedShortenLink = randomLink()
        }

        const checkUniqueLinkQuery = `*[_type == "link" && shortLink == $generatedShortenLink][0]`
        const checkUniqueLinkResult = decodeSanityPayload<any>(
          await apiConnection.fetch(checkUniqueLinkQuery, {
            generatedShortenLink,
          })
        )
        if (checkUniqueLinkResult) {
          isUnique = false
        } else {
          isUnique = true
        }
      }

      return generatedShortenLink as string
    } catch (error) {
      throw {
        code: 'general',
        message: 'Opps, something error when try to generated link',
      } as AwesomeError
    }
  }

  shortenUrl: (formData: any) => Promise<ShortenLink> = async (formData) => {
    try {
      const sourceLink = formData.link
      const customName = formData.customName
      const shortLink = await this.createRandomShortLink({
        customLink: customName,
      })
      const auth = await isAuthenticated()

      const input: CreateNewShortenLink = {
        id: v4(),
        sourceLink,
        status: 'active',
        type: 'link',
        shortLink,
        totalView: 0,
      }
      if (auth) {
        input.owner = { ref: auth.id, type: 'reference' }
      }

      const result = decodeSanityPayload<ShortenLink>(
        await apiConnection.create(encodeSanityPayload(input))
      )
      return result
    } catch (error) {
      if (typeof error == 'object') {
        throw error
      } else {
        throw {
          code: 'general',
          message: 'Opps, something error when try to shorten teh url',
        } as AwesomeError
      }
    }
  }
}

const linkService: LinkService = new LinkService()
export default linkService
