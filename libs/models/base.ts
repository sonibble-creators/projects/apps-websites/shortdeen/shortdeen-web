/**
 * # SanityBaseModel
 *
 * sanity base data model that mainly
 * use to extend the model in payload for sanity
 */
export interface SanityBaseModel {
  id: string
  type: string
  rev: string
  createdAt: string
  updatedAt: string
}

/**
 * # SanityInputBaseModel
 *
 * sanity base model that mainly use to extend the model
 * to create, update in sanity
 *
 */
export interface SanityInputBaseModel {
  id: string
  type: string
}

export interface SanityBaseReference {
  type?: string
  ref?: string
}

export interface SanityAssetBaseReference {
  type?: 'image' | 'file'
  asset: SanityBaseReference
}
