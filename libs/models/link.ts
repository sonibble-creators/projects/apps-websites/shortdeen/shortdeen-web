import {
  SanityBaseModel,
  SanityBaseReference,
  SanityInputBaseModel,
} from './base'

export interface ShortenLink extends SanityBaseModel {
  shortLink: string
  sourceLink: string
  totalView: number
  status: 'active' | 'disable'
  source?: string
}

export interface CreateNewShortenLink extends SanityInputBaseModel {
  shortLink: string
  sourceLink: string
  status: 'active' | 'disable'
  totalView?: number
  source?: string
  owner?: SanityBaseReference
}

export interface SummaryLink {
  totalLinks: number
  totalViews: number
}
