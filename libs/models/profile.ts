import {
  SanityAssetBaseReference,
  SanityBaseModel,
  SanityBaseReference,
  SanityInputBaseModel,
} from './base'
import { User } from './user'

export interface Profile extends SanityBaseModel {
  fullName?: string
  email?: string
  username?: string
  location?: string
  birthDate?: string
  bio?: string
  cover?: string
  avatar?: string
}

export interface CompleteProfile extends SanityInputBaseModel {
  fullName?: string
  email: string
  username: string
  location?: string
  birthDate?: string
  bio?: string
  avatar?: SanityAssetBaseReference
  cover?: SanityAssetBaseReference
}

export interface CreateInitialProfile extends SanityInputBaseModel {
  username: string
  user: SanityBaseReference
}

export interface Account {
  profile?: Profile
  user: User
}
