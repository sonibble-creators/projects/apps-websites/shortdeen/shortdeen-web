export interface AwesomeError {
  code:
    | 'auth/password-incorrect'
    | 'auth/password-invalid'
    | 'change-password/invalid-password'
    | 'change-password/new-password-not-match'
    | 'general'
  message: string
}
