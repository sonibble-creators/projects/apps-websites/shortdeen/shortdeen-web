import { SanityBaseModel, SanityInputBaseModel } from './base'

export interface User extends SanityBaseModel {
  identifier: string
  password: string
  status: 'active' | 'deactive'
  role: 'user' | 'creator' | 'admin'
  provider: 'google' | 'password'[]
}

export interface CreateUser extends SanityInputBaseModel {
  identifier: string
  password: string
  status: 'active' | 'deactive'
  role: 'user' | 'creator' | 'admin'
  provider: 'google' | 'password'[]
}
