import sanityClient, { ClientConfig, SanityClient } from '@sanity/client'

// check if the request is run on server
// or on the current browser
const isServerRequest = typeof window == 'undefined'

// the sanity configuration options
// use to handle the data and fecth some data through the sanity projects
const sanityConfig: ClientConfig = {
  projectId: isServerRequest
    ? process.env.SANITY_PROJECT_ID
    : process.env.NEXT_PUBLIC_SANITY_PROJECT_ID,
  dataset: isServerRequest
    ? process.env.SANITY_DATASET
    : process.env.NEXT_PUBLIC_SANITY_DATASET,
  apiVersion: isServerRequest
    ? process.env.SANITY_API_VERSION
    : process.env.NEXT_PUBLIC_SANITY_API_VERSION,
  token: isServerRequest
    ? process.env.SANITY_TOKEN
    : process.env.NEXT_PUBLIC_SANITY_TOKEN,
  useCdn: true,
  ignoreBrowserTokenWarning: true,
  withCredentials: true,
}

/**
 * # apiConnection
 *
 * this connection aim to become the gateway
 * and all of the feature of request
 * that connect to sanity studio
 */
const apiConnection: SanityClient = sanityClient(sanityConfig)

/**
 * # decodeSanityPayload
 *
 * decode the sanity payload become
 * the real object
 *
 * @param payload data passed to parse become
 * the decoded version
 *
 * @returns any
 */
const decodeSanityPayload: <
  T extends Record<string, any> | Record<string, any>
>(
  payload: Record<string, any> | Record<string, any>[]
) => T = (payload) => {
  let decodedPayload
  if (payload instanceof Array || Array.isArray(payload)) {
    decodedPayload = payload.map(
      (payl) =>
        (decodedPayload = JSON.parse(
          JSON.stringify(payl)
            .replace(/"_id":/g, '"id":')
            .replace(/"_type":/g, '"type":')
            .replace(/"_rev":/g, '"rev":')
            .replace(/"_ref":/g, '"ref":')
            .replace(/"_createdAt":/g, '"createdAt":')
            .replace(/"_updatedAt":/g, '"updatedAt":')
        ))
    )
  } else {
    decodedPayload = JSON.parse(
      JSON.stringify(payload)
        .replace(/"_id":/g, '"id":')
        .replace(/"_type":/g, '"type":')
        .replace(/"_rev":/g, '"rev":')
        .replace(/"_ref":/g, '"ref":')
        .replace(/"_createdAt":/g, '"createdAt":')
        .replace(/"_updatedAt":/g, '"updatedAt":')
    )
  }

  return decodedPayload as any
}

/**
 * # encodeSanityPayload
 *
 * encode the payload become sanity payload
 *
 * @param payload data passed to encode
 * @returns any
 */
const encodeSanityPayload: <T extends Record<string, any>>(
  payload: T
) => any = (payload) => {
  let encodedPayload
  encodedPayload = JSON.parse(
    JSON.stringify(payload)
      .replace(/"id":/g, '"_id":')
      .replace(/"type":/g, '"_type":')
      .replace(/"rev":/g, '"_rev":')
      .replace(/"ref":/g, '"_ref":')
  )

  return encodedPayload as any
}

export { apiConnection, encodeSanityPayload, decodeSanityPayload }
