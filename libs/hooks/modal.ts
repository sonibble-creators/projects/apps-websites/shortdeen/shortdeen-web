import { useQuery, useQueryClient, UseQueryResult } from '@tanstack/react-query'

interface UseModalResult {
  signIn: {
    listen: UseQueryResult<boolean | null | undefined>
    show: () => void
    hide: () => void
  }
  signOut: {
    listen: UseQueryResult<boolean | null | undefined>
    show: () => void
    hide: () => void
  }
}

/**
 * # useModal
 *
 * the hooks to manage the modal
 * and data showing
 *
 * @returns UseModalResult
 */
const useModal: () => UseModalResult = (): UseModalResult => {
  const queryClient = useQueryClient()

  return {
    signIn: {
      listen: useQuery<boolean | null | undefined>(
        ['modal', 'signin'],
        async () => {
          return false
        }
      ),
      show: () => {
        queryClient.setQueryData(['modal', 'signin'], true)
      },
      hide: () => {
        queryClient.setQueryData(['modal', 'signin'], false)
      },
    },
    signOut: {
      listen: useQuery<boolean | null | undefined>(
        ['modal', 'signout'],
        async () => {
          return false
        }
      ),
      show: () => {
        queryClient.setQueryData(['modal', 'signout'], true)
      },
      hide: () => {
        queryClient.setQueryData(['modal', 'signout'], false)
      },
    },
  }
}

export { useModal }
