import { useQuery, UseQueryResult } from '@tanstack/react-query'
import { withIronSessionSsr } from 'iron-session/next'
import { GetServerSidePropsContext, GetServerSidePropsResult } from 'next'

interface AuthUser {
  id: string
  identifier: string
  status: 'active' | 'deactive'
  role: 'user' | 'creator' | 'admin'
}

const AUTH_USER_KEY = 'auth'

/**
 * # sessionOptions
 *
 * the options for the session in server, or node
 */
const sessionOptions: any = {
  cookieName: '_cookie',
  password: process.env.COOKIE_SECRET ?? '',
  cookieOptions: {
    secure: process.env.NODE_ENV === 'production',
  },
}

/**
 * # isAuthenticated
 *
 * check the user and check if the user already authenticated
 * and get the data of current user using session
 * can be used in the browser or server
 *
 * @param context the server side context
 * @returns AuthUser
 */
const isAuthenticated: (
  context?: GetServerSidePropsContext
) => Promise<AuthUser | null> = async (context) => {
  if (typeof window == 'undefined' && context) {
    // authenticate from the server
    // like get server side props
    const user = context?.req.session.user
    return user
  }

  const body = {
    action: 'check',
  }

  const data = await fetch('/api/auth', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  }).then(async (res) => await res.json())
  if (data.id) {
    return data
  } else {
    return null
  }
}

/**
 * # unAuthenticate
 *
 * remove the current authenticatated user
 * and clear all of the session
 *
 */
const unAuthenticate: () => Promise<void> = async () => {
  const body = {
    action: 'logout',
  }

  await fetch('/api/auth', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  }).then(async (res) => await res.json())
}

/**
 * # authenticate
 *
 * authenticate the current user
 * by sending all of the information that needed in the session
 *
 * @param payload - the user information for authentication
 */
const authenticate: (payload: AuthUser) => Promise<void> = async (payload) => {
  const body = {
    action: 'signin',
    ...payload,
  }
  await fetch('/api/auth', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  }).then(async (res) => await res.json())
}

/**
 * # withAuthServer
 *
 * combine the server fetching with
 * the authentication of current user
 *
 * @param handler the context handler in the server
 * @returns GetServerSidePropsResult
 */
const withAuthServer: <
  P extends { [key: string]: unknown } = { [key: string]: unknown }
>(
  handler: (
    context: GetServerSidePropsContext
  ) => GetServerSidePropsResult<P> | Promise<GetServerSidePropsResult<P>>
) => (
  context: GetServerSidePropsContext
) => Promise<GetServerSidePropsResult<P>> = (handler) => {
  return withIronSessionSsr(handler, sessionOptions)
}

const checkAuthUser: () => Promise<AuthUser | null> = async () => {
  const body = {
    action: 'check',
  }

  const data = await fetch('/api/auth', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  }).then(async (res) => await res.json())
  if (data.id) {
    return data
  } else {
    return null
  }
}

/**
 * # useAuth
 *
 * the authentication hooks that will work on
 * the body of component that showing current authentication of the user
 *
 * @returns UseQueryResult<AuthUser>
 */
const useAuth: () => UseQueryResult<AuthUser | null> =
  (): UseQueryResult<AuthUser | null> => {
    return useQuery<AuthUser | null>([AUTH_USER_KEY], checkAuthUser)
  }

export {
  sessionOptions,
  useAuth,
  isAuthenticated,
  authenticate,
  unAuthenticate,
  withAuthServer,
}
