import { useQuery, useQueryClient, UseQueryResult } from '@tanstack/react-query'
import { useEffect } from 'react'

const USE_THEME_KEY = 'theme'

interface UseThemeData {
  mode: 'light' | 'dark'
  device: 'dekstop' | 'mobile' | 'tablet'
}

/**
 * checkThemeDevice
 *
 * check the device theme
 * that'f for mobile. desktop, and tablet
 *
 * @param update the function to show the update
 */
const checkThemeDevice: (update: (mode: any) => void) => void = (update) => {
  if (typeof window != 'undefined') {
    if (window.matchMedia('(min-width: 640px)').matches) {
      update('tablet')
    } else if (window.matchMedia('(min-width: 1024px )').matches) {
      update('desktop')
    }
  } else {
    update('mobile')
  }
}

/**
 *  # checkThemeModeChange
 *
 * check the theme mode
 * with subscribe for change
 *
 * @param update the function that show with the update
 */
const checkThemeMode: (update: (mode: any) => void) => void = (update) => {
  if (typeof window != 'undefined') {
    if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
      update('dark')
    }

    window
      .matchMedia('(prefers-color-scheme: dark)')
      .addEventListener('change', (e) => {
        if (e.matches) {
          update('dark')
        } else {
          update('light')
        }
      })
  } else {
    update('light')
  }
}

/**
 * # useTheme
 *
 * check the theme and
 * handle the state for theme
 *
 * @returns UseQueryResult<UseThemeData>
 */
const useTheme: () => UseQueryResult<UseThemeData> = () => {
  const queryClient = useQueryClient()

  useEffect(() => {
    checkThemeMode((mode) => {
      queryClient.setQueryData<UseThemeData>([USE_THEME_KEY], (oldData) => {
        return { ...oldData, mode } as any
      })
    })
    checkThemeDevice((device) => {
      queryClient.setQueryData<UseThemeData>([USE_THEME_KEY], (oldData) => {
        return { ...oldData, device } as any
      })
    })
  }, [])

  return useQuery<UseThemeData>([USE_THEME_KEY], () => {
    return { mode: 'light', device: 'mobile' } as any
  })
}

export default useTheme
export { checkThemeDevice, checkThemeMode }
