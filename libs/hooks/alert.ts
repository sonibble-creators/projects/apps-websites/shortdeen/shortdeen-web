import { useQueryClient } from '@tanstack/react-query'

const ALERT_MODAL_KEY = 'alert-modal'

interface UseAlertShowParams {
  type: 'error' | 'success'
  title: string
  description: string
}

interface UseAlertResult {
  show: (params: UseAlertShowParams) => void
  hide: () => void
}

/**
 *  # useAlert
 *
 * the hooks to handle the alert modal
 * use to show and hide the current alert to the application
 *
 * @returns UseAlertResult
 */
const useAlert: () => UseAlertResult = () => {
  const queryClient = useQueryClient()

  // show the alert
  const showAlert: (params: UseAlertShowParams) => void = ({
    type,
    title,
    description,
  }) => {
    queryClient.setQueryData([ALERT_MODAL_KEY], { type, title, description })
  }

  // hide the alert
  const hideAlert: () => void = () => {
    queryClient.setQueryData([ALERT_MODAL_KEY], null)
  }

  return { show: showAlert, hide: hideAlert }
}

export { useAlert }
