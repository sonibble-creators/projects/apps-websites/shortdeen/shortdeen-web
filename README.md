<div id="top"></div>
<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/sonibble-creators/projects/apps-websites/shortdeen/shortdeen-web">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/40409038/Logo_-_White.png" alt="Logo" width="100" height="100">
  </a>

  <h1 align="center">Shortdeen - Web</h1>

  <p align="center">
Stop wasting time searching for shortener url
    <br />
    <a href="https://gitlab.com/sonibble-creators/projects/apps-websites/shortdeen/shortdeen-web/-/wikis"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://shortdeen-web.vercel.app">View Demo</a>
    ·
    <a href="https://gitlab.com/sonibble-creators/projects/apps-websites/shortdeen/shortdeen-web/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/sonibble-creators/projects/apps-websites/shortdeen/shortdeen-web/-/issues">Request Feature</a>
  </p>
</div>

<br/>
<br/>

<!-- Pipeline status -->
<div align="center">

<a href="https://gitlab.com/sonibble-creators/projects/apps-websites/shortdeen/shortdeen-web/-/commits/main"><img alt="pipeline status" src="https://gitlab.com/sonibble-creators/projects/apps-websites/shortdeen/shortdeen-web/badges/main/pipeline.svg" /></a>

<a href="https://gitlab.com/sonibble-creators/projects/apps-websites/shortdeen/shortdeen-web/-/commits/main"><img alt="coverage report" src="https://gitlab.com/sonibble-creators/projects/apps-websites/shortdeen/shortdeen-web/badges/main/coverage.svg" /></a>

<a href="https://gitlab.com/sonibble-creators/projects/apps-websites/shortdeen/shortdeen-web/-/releases"><img alt="Latest Release" src="https://gitlab.com/sonibble-creators/projects/apps-websites/shortdeen/shortdeen-web/-/badges/release.svg" /></a>

</div>

<br />
<br />

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#about-the-project">About The Project</a></li>
    <li><a href="#getting-started">Get Started</a></li>
    <li><a href="#wiki">Wiki</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<br />

<!-- ABOUT THE PROJECT -->
<!-- all about the project, specify the background -->

## About The Project

The Shortener url for simplicity and customizable. This project build using unique idea that define about help people to shorten the link and easy to claim

This function allow to shorten the url and share it through social media. Also include monitoring feature So you can easily see the current statistic.

<br/>
<br/>

### Stacks

There's some stack and technology used in this project that will bring the development into the high level:

1. [NextJS](https://nextjs.org)
2. [Tailwind Css](https://tailwindcss.com)
3. [EmailJS](https://emailjs.com)
4. [GSAP](https://greenshock.com)
5. [ReactQuery](https://tanstack.com/query/v4)
6. [ZOD](https://github.com/colinhacks/zod)
7. [React Hook Form](https://react-hook-form.com)
8. [React Awesome Cursor](https://gitlab.com/sonibble-creators/products/plugins-addons/react-awesome-cursor)
9. [Sanity.io](https://sanity.io)

<br/>
<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

## Gettting Started

This project use the node module to install some dependencies to work. So please ensure you conplete all requirement stacks

### Requirements

- NodeJS (Version 16 UP)

  <br/>
<br/>

### Instalation

1. First install all of the dependencies

```bash

# npm
# install all dependencies and download it
npm install --legacy-peer-deps

# or yarn
yarn install

```

2.  After all dependencies installed. Now let's start the server to running the application.

```bash

# start the server in development mode (watch)
npm run dev

# start application from build version
npm run start

```

3.  Now you're ready to develop. then open the browser [http://localhost:3000](http://localhost:3000)

 <br/>
 <br/>

<!-- Wiki -->
<!-- enable the user to see the wiki of this project -->

## Wiki

We build this project with some record of our documentation, If you interest to see the all about this project please check the wiki.

_For more detail, please refer to the [Wiki](https://gitlab.com/sonibble-creators/projects/apps-websites/shortdeen/shortdeen-web/-/wikis)_

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- ROADMAP -->
<!-- Initial info roadmap of this project -->

## Roadmap

- [x] Complete simple and modern design
- [x] Fully support dark mode
- [x] Fully responsive support
- [x] SEO Friendly
- [x] Simple Shorten Url

See the [open issues](https://gitlab.com/sonibble-creators/projects/apps-websites/shortdeen/shortdeen-web/-/issues) for a full list of proposed features (and known issues).

Refere to changelog to see the detail [CHANGELOG](CHANGELOG.md)

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".

Please check the contributing procedure [here](CONTRIBUTING.md), Don't forget to give the project a star! Thanks again!

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- LICENSE -->

## License

This project is not distributed on any license. So please do contact when need to take on this project

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- CONTACT -->

## Contact

Nyoman Sunima - [@nyomansunima](https://instagram.com/nyomansunima) - nyomansunima@gmail.com

Sonibble - [@sonibble](https://instagram.com/sonibble) - [creative.sonibble@gmail.com](mailto:creative.sonibble@gmail.com) - [@sonibble](https://twitter.com/sonibble)

Project Link: [Contact Email Template](https://gitlab.com/sonibble-creators/projects/apps-websites/shortdeen/shortdeen-web)

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>
