import { AwesomeButton, FlatButton } from '@components/base/button'
import { Form } from '@components/base/form'
import { PasswordInput, TextInput } from '@components/base/input'
import { zodResolver } from '@hookform/resolvers/zod'
import { useAlert } from '@hooks/alert'
import { authenticate } from '@hooks/auth'
import { useModal } from '@hooks/modal'
import { AwesomeError } from '@models/error'
import accountService from '@services/account'
import { useMutation } from '@tanstack/react-query'
import { useRouter } from 'next/router'
import { FunctionComponent, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { z } from 'zod'

const signInFormSchema = z.object({
  identifier: z.string().min(1, {
    message: 'Please fill the identifier like username, email or password',
  }),
  password: z
    .string()
    .min(8, { message: 'Password must at least 8 characters long' }),
})

/**
 * # SignInModal
 *
 * modal to showing the form
 * for sign in
 *
 * @returns JSX.Element
 */
const SignInModal: FunctionComponent = (): JSX.Element => {
  const signInForm = useForm({ resolver: zodResolver(signInFormSchema) })
  const {
    signIn: {
      hide,
      listen: { data },
    },
  } = useModal()
  const alert = useAlert()
  const router = useRouter()

  const signIn = useMutation(accountService.signIn, {
    onSuccess: async (data, variables, context) => {
      alert.show({
        type: 'success',
        title: 'Signed In',
        description: 'You already login to the land',
      })
      await authenticate({
        id: data.id,
        identifier: data.identifier,
        role: data.role,
        status: data.status,
      })
      hide()
      setTimeout(() => {
        router.push('/settings/profile')
      }, 500)
    },
    onError: (error: AwesomeError, variables, context) => {
      if (error.code == 'auth/password-incorrect') {
        signInForm.setError('password', { message: error.message })
      } else {
        alert.show({
          type: 'error',
          title: 'Opps',
          description: error.message,
        })
      }
    },
  })

  const onFormSubmitted: (formData: any) => void = (formData) => {
    signIn.mutate(formData)
  }

  const closeModalOnEscapeDown: (e: KeyboardEvent) => void = (e) => {
    if (e.code == 'Escape') {
      hide()
    }
  }

  // close the moda
  // based on the key
  useEffect(() => {
    document.body.addEventListener('keydown', closeModalOnEscapeDown)

    return () => {
      document.body.removeEventListener('keydown', closeModalOnEscapeDown)
    }
  }, [data])

  return (
    <>
      <div
        className={`${
          data
            ? 'flex fixed z-[62] inset-0 pointer-events-auto'
            : 'h-0 relative overflow-hidden pointer-events-none'
        }`}
      >
        <div
          className={`${
            data ? 'opacity-100' : 'opacity-0'
          } flex absolute inset-0 bg-black dark:bg-white bg-opacity-10 dark:bg-opacity-10 backdrop-blur-md transition-all duration-300`}
          onClick={hide}
        ></div>
        <div className='flex relative mx-auto my-auto container justify-center items-center'>
          <div
            className={`${
              data
                ? 'opacity-100 scale-100 translate-y-0'
                : 'opacity-0 scale-50 translate-y-full'
            } flex w-6/12 bg-white dark:bg-black p-10 rounded-[40px] flex-col transition-all duration-500 delay-100 hover:-translate-y-2 hover:scale-105`}
            onClick={(e) => e.stopPropagation()}
          >
            <h3 className='text-heading-3 font-medium'>Sign In.</h3>

            <Form
              onSubmit={onFormSubmitted}
              context={signInForm}
              className='mt-16'
              id='signin-form'
            >
              <TextInput
                name='identifier'
                placeholder='Your username, email or phone'
              />
              <PasswordInput name='password' placeholder='Your password' />
            </Form>

            <FlatButton
              disable={signIn.isLoading}
              form='signin-form'
              className='mt-10'
            >
              {signIn.isLoading ? (
                <>
                  Signing in
                  <i className='fi fi-rr-spinner flex justify-center items-center text-[24px] absolute right-0 animate-spin' />
                </>
              ) : (
                <>
                  Sign In
                  <i className='fi fi-rr-arrow-right flex justify-center items-center text-[24px] absolute right-0' />
                </>
              )}
            </FlatButton>
          </div>
        </div>
      </div>
    </>
  )
}

export default SignInModal
