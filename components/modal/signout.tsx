import { AwesomeButton, FlatButton } from '@components/base/button'
import { Form } from '@components/base/form'
import { PasswordInput, TextInput } from '@components/base/input'
import { zodResolver } from '@hookform/resolvers/zod'
import { useAlert } from '@hooks/alert'
import { authenticate, unAuthenticate } from '@hooks/auth'
import { useModal } from '@hooks/modal'
import { AwesomeError } from '@models/error'
import accountService from '@services/account'
import { useMutation } from '@tanstack/react-query'
import { useRouter } from 'next/router'
import { FunctionComponent, useEffect } from 'react'

/**
 * # SignOutModal
 *
 * modal to showing the form
 * for sign in
 *
 * @returns JSX.Element
 */
const SignOutModal: FunctionComponent = (): JSX.Element => {
  const {
    signOut: {
      hide,
      listen: { data },
    },
  } = useModal()
  const alert = useAlert()
  const router = useRouter()

  const signOut = useMutation(accountService.signOut, {
    onSuccess: () => {
      alert.show({
        type: 'success',
        title: 'Signed Out',
        description: 'You already logout from the land',
      })
      hide()
      setTimeout(async () => {
        await router.push('/')
        router.reload()
      }, 500)
    },
    onError: (error: AwesomeError, variables, context) => {
      alert.show({
        type: 'error',
        title: 'Opps',
        description: error.message,
      })
      hide()
    },
  })

  const onSignOut: () => void = () => {
    signOut.mutate()
  }

  const closeModalOnEscapeDown: (e: KeyboardEvent) => void = (e) => {
    if (e.code == 'Escape') {
      hide()
    }
  }

  // close the moda
  // based on the key
  useEffect(() => {
    document.body.addEventListener('keydown', closeModalOnEscapeDown)

    return () => {
      document.body.removeEventListener('keydown', closeModalOnEscapeDown)
    }
  }, [data])

  return (
    <>
      <div
        className={`${
          data
            ? 'flex fixed z-[62] inset-0 pointer-events-auto'
            : 'h-0 relative overflow-hidden pointer-events-none'
        }`}
      >
        <div
          className={`${
            data ? 'opacity-100' : 'opacity-0'
          } flex absolute inset-0 bg-black dark:bg-white bg-opacity-10 dark:bg-opacity-10 backdrop-blur-md transition-all duration-300`}
          onClick={hide}
        ></div>
        <div className='flex relative mx-auto my-auto container justify-center items-center'>
          <div
            className={`${
              data
                ? 'opacity-100 scale-100 translate-y-0'
                : 'opacity-0 scale-50 translate-y-full'
            } flex w-6/12 bg-white dark:bg-black p-10 rounded-[40px] flex-col transition-all duration-500 delay-100 hover:-translate-y-2 hover:scale-105`}
            onClick={(e) => e.stopPropagation()}
          >
            <h3 className='text-heading-3 font-medium'>Sign Out</h3>

            <span className='flex mt-14'>
              After signout you will no longer abble to see the dashboard and
              all of the creator feature to see all of the links
            </span>

            <FlatButton
              disable={signOut.isLoading}
              type='button'
              onClick={onSignOut}
              className='mt-10'
            >
              {signOut.isLoading ? (
                <>
                  Signing out
                  <i className='fi fi-rr-spinner flex justify-center items-center text-[24px] absolute right-0 animate-spin' />
                </>
              ) : (
                <>
                  Sign Out
                  <i className='fi fi-rr-arrow-right flex justify-center items-center text-[24px] absolute right-0' />
                </>
              )}
            </FlatButton>
          </div>
        </div>
      </div>
    </>
  )
}

export default SignOutModal
