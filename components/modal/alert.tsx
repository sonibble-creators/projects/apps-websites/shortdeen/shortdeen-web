import { useAlert } from '@libs/hooks/alert'
import { useQuery } from '@tanstack/react-query'
import { FunctionComponent, useEffect } from 'react'

interface AlertData {
  type: 'error' | 'success'
  title?: string
  description: string
}

/**
 * # AlertModal
 *
 * the modal for alert
 * showing some info about the problem and status of actions
 *
 * @returns JSX.Element
 */
const AlertModal: FunctionComponent = (): JSX.Element => {
  const { data } = useQuery<AlertData | null | undefined>(
    ['alert-modal'],
    () => null
  )
  const alert = useAlert()

  // auto close the alert
  useEffect(() => {
    if (data) {
      setTimeout(() => alert.hide(), 3000)
    }
  }, [data])

  return (
    <>
      {data && (
        <div className='flex items-center gap-5 fixed bottom-20 right-20 z-[68] bg-white dark:bg-black rounded-3xl p-5 max-w-md cursor-none transition-all duration-500 hover:scale-95'>
          <div
            className={`flex justify-center items-center bg-opacity-10 h-14 w-14 rounded-full ${
              data.type == 'error' ? 'text-red bg-red' : 'text-green bg-green'
            }`}
          >
            <i
              className={`${
                data.type == 'error'
                  ? 'fi fi-rr-exclamation '
                  : 'fi fi-rr-check '
              } flex justify-center items-center text-[24px]`}
            ></i>
          </div>
          <div className='flex flex-1 flex-col gap-2'>
            <h6 className='text-subtitle-1 font-medium'>{data.title}</h6>
            <span className='text-body-2 text-gray-dark dark:text-gray-normal'>
              {data.description}
            </span>
          </div>
        </div>
      )}
    </>
  )
}

export default AlertModal
