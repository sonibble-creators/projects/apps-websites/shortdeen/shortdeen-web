import { FunctionComponent, ReactElement } from 'react'
import MainFooter from '../footer/main'
import MainHeader from '../header/main'
import SettingNav from '../navs/setting'

interface SettingLayoutProps {
  children?: JSX.Element | ReactElement | ReactElement
}

/**
 * # SettingLayout
 *
 * the layout to handle the setting page
 * including the account, profile and even the change password privacy
 *
 * @returns JSX.Element
 */
const SettingLayout: FunctionComponent<SettingLayoutProps> = ({
  children,
}): JSX.Element => {
  return (
    <>
      <MainHeader />
      <main className='flex container mx-auto py-52 gap-16'>
        <SettingNav />
        <div className='flex flex-col w-full px-5 desktop:px-0 desktop:w-7/12'>
          {children}
        </div>
      </main>
      <MainFooter />
    </>
  )
}

export default SettingLayout
