import { FunctionComponent, ReactElement, ReactNode } from 'react'
import MainFooter from '../footer/main'
import SimpleHeader from '../header/simple'

interface GeneralLayoutProps {
  children?: ReactElement | ReactNode | JSX.Element | JSX.Element[]
}

/**
 * # GeneralLayout
 *
 * the general purposed layout
 * that contain the header and footer
 * in some pages like home, about, contact, privacy, etc.
 *
 * @returns JSX.Element
 */
const GeneralLayout: FunctionComponent<GeneralLayoutProps> = ({
  children,
}): JSX.Element => {
  return (
    <>
      <SimpleHeader />
      {children}
      <MainFooter />
    </>
  )
}

export default GeneralLayout
