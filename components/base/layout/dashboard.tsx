import { FunctionComponent, ReactElement, ReactNode } from 'react'
import MainHeader from '../header/main'
import DashboardNav from '../navs/dashboard'

interface DashboardLayoutProps {
  children?: ReactElement | ReactNode | JSX.Element
}

const DashboardLayout: FunctionComponent<DashboardLayoutProps> = ({
  children,
}): JSX.Element => {
  return (
    <>
      <MainHeader />
      <main className='flex container desktop:ml-[300px] mx-auto large-desktop:ml-[300px] flex-col py-40 px-5 desktop:px-0'>
        {children}
      </main>
      <DashboardNav />
    </>
  )
}

export default DashboardLayout
