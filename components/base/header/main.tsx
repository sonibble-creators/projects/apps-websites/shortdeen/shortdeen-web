import { useModal } from '@hooks/modal'
import useTheme from '@hooks/theme'
import accountService from '@services/account'
import { useQuery } from '@tanstack/react-query'
import Image from 'next/image'
import Link from 'next/link'
import { FunctionComponent } from 'react'

interface AvatarMenuItem {
  label: string
  link?: string
  icon: string
}

const avatarMenus: AvatarMenuItem[] = [
  { label: 'Profile', link: '/settings/profile', icon: 'fi fi-rr-user' },
  { label: 'Settings', link: '/settings', icon: 'fi fi-rr-settings' },
]

/**
 * # MainHeader
 *
 * the main header that contain some of the element that showing
 * in explore, admin, and creators
 *
 * @returns JSX.Element
 */
const MainHeader: FunctionComponent = (): JSX.Element => {
  const account = useQuery(['account'], accountService.loadAccount)
  const theme = useTheme()
  const modal = useModal()

  const signOut: () => void = () => {
    modal.signOut.show()
  }

  return (
    <header
      className='flex py-8 px-[9%] fixed inset-x-0 top-0 z-[61] items-center justify-between'
      id='main-header'
    >
      <div className='flex'>
        <Link href={'/'}>
          <div
            className='flex items-center gap-4 cursor-pointer'
            data-cursor-size='100'
            data-cursor-exclusion
          >
            {/* The Logo */}
            <svg
              width='48'
              height='46'
              viewBox='0 0 48 46'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                d='M20.1264 12.58C22.0379 9.94746 25.9621 9.94746 27.8736 12.58L39.2665 28.2708C41.5643 31.4353 39.3036 35.8705 35.3929 35.8705H12.6071C8.6964 35.8705 6.43573 31.4353 8.73345 28.2708L20.1264 12.58Z'
                className='fill-primary'
              />
              <path
                d='M20.442 34.415H27.5579V39.6549C27.5579 41.6199 25.965 43.2129 24 43.2129V43.2129C22.035 43.2129 20.442 41.6199 20.442 39.6549V34.415Z'
                className='fill-primary'
              />
              <path
                d='M20.1264 5.33493C22.0379 2.70234 25.9621 2.70234 27.8736 5.33493L39.2665 21.0257C41.5643 24.1902 39.3036 28.6253 35.3929 28.6253H12.6071C8.6964 28.6253 6.43573 24.1902 8.73345 21.0257L20.1264 5.33493Z'
                className='fill-primary'
              />
              <rect
                x='20.442'
                y='27.1699'
                width='7.1159'
                height='8.66846'
                className='fill-primary'
              />
              <mask
                id='mask0_23_692'
                maskUnits='userSpaceOnUse'
                x='7'
                y='3'
                width='34'
                height='33'
              >
                <path
                  d='M20.1264 5.33493C22.0379 2.70234 25.9621 2.70234 27.8736 5.33493L39.2665 21.0257C41.5643 24.1902 39.3036 28.6253 35.3929 28.6253H12.6071C8.6964 28.6253 6.43573 24.1902 8.73345 21.0257L20.1264 5.33493Z'
                  fill='#80FFA3'
                />
                <rect
                  x='20.442'
                  y='27.1699'
                  width='7.1159'
                  height='8.66846'
                  fill='#80FFA3'
                />
              </mask>
              <g mask='url(#mask0_23_692)'>
                <path
                  d='M21.8014 10.2731C22.8863 8.77888 25.1137 8.77888 26.1986 10.273L41.6527 31.5571C42.9568 33.3532 41.6738 35.8705 39.4542 35.8705H8.54582C6.32623 35.8705 5.04315 33.3532 6.34727 31.5571L21.8014 10.2731Z'
                  className={
                    theme.data?.mode == 'dark' ? 'fill-white' : 'fill-black'
                  }
                />
                <rect
                  x='20.442'
                  y='35.3208'
                  width='7.1159'
                  height='8.66846'
                  className={
                    theme.data?.mode == 'dark' ? 'fill-white' : 'fill-black'
                  }
                />
              </g>
            </svg>

            {/* The Brand Name */}
            <h1 className='text-heading-6 font-medium font-poppins'>
              Shortdeen
            </h1>
          </div>
        </Link>
      </div>

      <div className='flex flex-1'></div>

      {/* account action */}
      <div className='flex items-center gap-16'>
        <div className='flex relative group'>
          <div className='flex h-12 w-12 rounded-full overflow-hidden relative cursor-pointer'>
            <Image
              src={
                account.data?.profile?.avatar ??
                '/assets/image/avatar-placeholder.png'
              }
              layout='fill'
              objectFit='cover'
              priority
              alt='user Avatar'
            />
          </div>

          <div className='flex flex-col absolute opacity-0 translate-x-full scale-50 group-hover:scale-100 group-hover:translate-x-0 group-hover:opacity-100 transition-all duration-500 delay-100 z-[60] top-20 right-0 w-[280px] p-5 rounded-2xl bg-white dark:bg-black border-gray-light dark:border-opacity-10'>
            <div className='flex flex-col cursor-none'>
              <span className='flex text-subtitle-normal font-medium'>
                {account.data?.profile?.fullName}
              </span>
              <span className='flex text-labels text-gray-dark'>
                {`@${account.data?.profile?.username}`}
              </span>
            </div>
            <div className='flex border-t border-t-gray-light mt-5  pt-3'>
              <ul className='flex flex-col list-none gap-2'>
                {avatarMenus.map(({ icon, label, link }, position) => (
                  <li
                    className='flex transition-all duration-300 hover:-translate-x-1 cursor-pointer dark:text-gray-normal'
                    key={position}
                  >
                    <Link href={link ?? ''}>
                      <div className='flex gap-4 py-1'>
                        <i
                          className={`${icon} flex justify-center items-center text-[20px]`}
                        ></i>
                        <span className='flex'>{label}</span>
                      </div>
                    </Link>
                  </li>
                ))}
                <li
                  onClick={signOut}
                  className='flex transition-all duration-300 hover:-translate-x-1 cursor-pointer'
                >
                  <div className='flex gap-4 py-1'>
                    <i
                      className={`fi fi-rr-exit flex justify-center items-center text-[20px]`}
                    ></i>
                    <span className='flex'>Log Out</span>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </header>
  )
}

export default MainHeader
