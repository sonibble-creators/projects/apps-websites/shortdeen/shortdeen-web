import Link from 'next/link'
import { useRouter } from 'next/router'
import { FunctionComponent, useEffect, useState } from 'react'

interface NavItem {
  icon: string
  label: string
  link: string
}

const primaryNavMenus: NavItem[] = [
  {
    icon: 'fi fi-rr-chart-pie-alt',
    label: 'Summary',
    link: '/dashboard/summary',
  },
  {
    icon: 'fi fi-rr-link',
    label: 'Links',
    link: '/dashboard/links',
  },
]

const secondaryNavMenus: NavItem[] = [
  {
    icon: 'fi fi-rr-settings-sliders',
    label: 'Settings',
    link: '/settings/profile',
  },
  {
    icon: 'fi fi-rr-shield',
    label: 'Privacy',
    link: '/privacy',
  },
]

/**
 * # DashboardNav
 *
 * the navigation for dashboard
 * that allow to manage all of the menu inside the dashboard func.
 *
 * @returns JSX.Element
 */
const DashboardNav: FunctionComponent = (): JSX.Element => {
  const router = useRouter()
  const [activeMenu, setActiveMenu] = useState<string>()
  const [isShowMenuMobile, setShowMenuMobile] = useState<boolean>(false)

  const toggleMobileMenu: () => void = () => {
    setShowMenuMobile((isShow) => !isShow)
  }

  useEffect(() => {
    const currentLink = router.asPath
    setActiveMenu(currentLink)
  }, [router.asPath])

  return (
    <div className='flex flex-col fixed z-50 bottom-0 inset-x-0 desktop:inset-y-0 desktop:h-screen desktop:left-10 px-5 py-4 border-t desktop:border-none desktop:pt-40 desktop:pb-14 border-t-gray-light dark:border-t-gray-dark dark:border-opacity-30 bg-white dark:bg-black desktop:w-[240px]'>
      <div
        className={`${
          isShowMenuMobile ? 'flex' : 'hidden'
        } desktop:!flex flex-col gap-10`}
      >
        <span className='text-gray-dark text-body-normal'>ALL YOU NEED</span>
        <nav className='flex flex-col'>
          <ul className='flex flex-col list-none gap-5'>
            {primaryNavMenus.map(({ icon, label, link }, position) => (
              <li
                className='flex transition-all duration-300 hover:-translate-x-2 cursor-pointer group'
                key={position}
              >
                <Link href={link}>
                  <div className='flex gap-4 items-center'>
                    <div className='flex h-14 w-14 rounded-[20px] justify-center items-center border border-gray-light dark:border-gray-dark dark:border-opacity-40 relative overflow-hidden'>
                      <span
                        className={`bg-black dark:bg-white rounded-[20px] absolute inset-0 transition-all duration-500 -translate-x-[110%] group-hover:translate-x-0 ${
                          activeMenu == link ? 'translate-x-0' : ''
                        }`}
                      ></span>
                      <i
                        className={`${icon} flex justify-center items-center text-[20px] relative transition-all duration-300 delay-100 group-hover:text-white dark:group-hover:text-black ${
                          activeMenu == link ? 'text-white dark:text-black' : ''
                        }`}
                      ></i>
                    </div>
                    <span className='text-body-normal font-medium'>
                      {label}
                    </span>
                  </div>
                </Link>
              </li>
            ))}
          </ul>
        </nav>
      </div>
      <div className='flex flex-1'></div>
      <div
        className={`${
          isShowMenuMobile ? 'flex' : 'hidden'
        } desktop:!flex flex-col gap-10 mt-20 desktop:mt-0`}
      >
        <span className='text-gray-dark text-body-normal'>SOME HELP</span>
        <nav className='flex flex-col'>
          <ul className='flex flex-col list-none gap-5'>
            {secondaryNavMenus.map(({ icon, label, link }, position) => (
              <li
                className='flex transition-all duration-300 hover:-translate-x-2 cursor-pointer group'
                key={position}
              >
                <Link href={link}>
                  <div className='flex gap-4 items-center'>
                    <div className='flex h-14 w-14 rounded-[20px] justify-center items-center border border-gray-light dark:border-gray-dark dark:border-opacity-40 relative overflow-hidden'>
                      <span
                        className={`bg-black dark:bg-white rounded-[20px] absolute inset-0 transition-all duration-500 -translate-x-[110%] group-hover:translate-x-0 ${
                          activeMenu == link ? 'translate-x-0' : ''
                        }`}
                      ></span>
                      <i
                        className={`${icon} flex justify-center items-center text-[20px] relative transition-all duration-300 delay-100 group-hover:text-white dark:group-hover:text-black ${
                          activeMenu == link ? 'text-white dark:text-black' : ''
                        }`}
                      ></i>
                    </div>
                    <span className='text-body-normal font-medium'>
                      {label}
                    </span>
                  </div>
                </Link>
              </li>
            ))}
          </ul>
        </nav>
      </div>

      <button
        onClick={toggleMobileMenu}
        className='flex desktop:hidden h-12 w-12 rounded-2xl justify-center items-center bg-white dark:bg-black border border-gray-light dark:border-gray-dark dark:border-opacity-40 absolute right-5 -top-8'
      >
        <i
          className={`fi fi-rr-arrow-small-${
            isShowMenuMobile ? 'down' : 'up'
          } flex justify-center items-center text-[18px]`}
        ></i>
      </button>
    </div>
  )
}

export default DashboardNav
