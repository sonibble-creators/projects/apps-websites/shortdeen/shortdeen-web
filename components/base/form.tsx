import { FunctionComponent } from 'react'
import {
  FormProvider,
  useForm,
  UseFormReturn,
  ValidationMode,
} from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'

interface FormProps {
  id?: string
  onChange?: () => void
  onSubmit?: (formData: any) => void
  context?: UseFormReturn
  className?: string
  resolverSchema?: any
  children?:
    | string
    | string[]
    | JSX.Element
    | JSX.Element[]
    | ((context: UseFormReturn) => JSX.Element | JSX.Element[])
  mode?: 'all' | 'onBlur' | 'onChange' | 'onSubmit' | 'onTouched'
}

/**
 * # Form
 *
 * the base for that use with the hooks
 * contain the form context that can use with the children
 *
 * @returns JSX.Element
 */
const Form: FunctionComponent<FormProps> = ({
  id,
  className,
  onSubmit,
  onChange,
  resolverSchema,
  children,
  context,
  mode = 'onSubmit',
}): JSX.Element => {
  const formMethods =
    context ??
    useForm({
      resolver: resolverSchema && zodResolver(resolverSchema),
      mode,
    })

  const handleFormSubmit: (formData: any) => void = (formData) => {
    if (onSubmit) {
      onSubmit(formData)
    }
  }

  const handleFormChange: () => void = () => {
    if (onChange) {
      onChange()
    }
  }

  return (
    <FormProvider {...formMethods}>
      <form
        className={`flex flex-col gap-6 w-full ${className}`}
        method='POST'
        onSubmit={formMethods.handleSubmit(handleFormSubmit)}
        onChange={handleFormChange}
        id={id}
      >
        {typeof children == 'function' ? children(formMethods) : children}
      </form>
    </FormProvider>
  )
}

export { Form }
