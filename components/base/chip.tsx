import { FunctionComponent, ReactElement, ReactNode } from 'react'

interface AwesomeChipProps {
  children?:
    | string
    | string[]
    | JSX.Element
    | JSX.Element[]
    | ReactElement
    | ReactNode
  isSelected?: boolean
  onClick?: () => void
  className?: string
}

/**
 * # AwesomeChip
 *
 * the chip to showing some option using special chipd
 * that can be selected
 *
 * @returns JSX.Element
 */
const AwesomeChip: FunctionComponent<AwesomeChipProps> = ({
  children,
  isSelected,
  onClick,
  className,
}): JSX.Element => {
  return (
    <div
      onClick={onClick}
      className={`flex relative px-8 py-5 justify-center items-center rounded-full overflow-hidden text-button font-medium border border-gray-normal dark:border-gray-darker dark:border-opacity-30 group transition-all duration-300 hover:scale-95 cursor-pointer ${className}`}
    >
      <span
        className={`absolute h-full w-full rounded-full bg-black dark:bg-white transition-all duration-500 group-hover:translate-x-0 group-disabled:group-hover:-translate-x-full ${
          isSelected ? 'translate-x-0' : '-translate-x-[110%]'
        }`}
      ></span>
      <span
        className={`relative gap-4 flex justify-center items-center group-hover:text-white dark:group-hover:text-black transition-all duration-300 delay-100 ${
          isSelected
            ? 'text-white dark:text-black'
            : 'text-black dark:text-white'
        }`}
      >
        {children}
      </span>
    </div>
  )
}

export { AwesomeChip }
