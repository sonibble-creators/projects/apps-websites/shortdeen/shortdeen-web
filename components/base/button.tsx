import { FunctionComponent, ReactElement, ReactNode } from 'react'

interface BaseButtonProps {
  children?:
    | string
    | string[]
    | JSX.Element
    | JSX.Element[]
    | ReactElement
    | ReactNode
  disable?: boolean
  onClick?: () => void
  form?: string
  className?: string
  type?: 'submit' | 'button'
}

interface AwesomeButtonProps extends BaseButtonProps {}

interface FlatButtonProps extends BaseButtonProps {}

/**
 * # AwesomeButton
 *
 * the main button to showing
 * the button with awesome way
 *
 * @returns JSX.Element
 */
const AwesomeButton: FunctionComponent<AwesomeButtonProps> = ({
  children,
  disable,
  onClick,
  form,
  className,
  type = 'button',
}): JSX.Element => {
  return (
    <button
      onClick={onClick}
      disabled={disable}
      type={type ?? form ? 'submit' : 'button'}
      form={form}
      className={`flex relative px-8 py-5 justify-center items-center rounded-full overflow-hidden text-button font-medium border border-gray-normal dark:border-gray-darker dark:border-opacity-30 group transition-all duration-300 hover:scale-95 ${className}`}
    >
      <span className='absolute h-full w-full rounded-full bg-black dark:bg-white transition-all duration-500 -translate-x-[110%] group-hover:translate-x-0 group-disabled:group-hover:-translate-x-full'></span>
      <span className='relative gap-4 flex w-full justify-center items-center text-black dark:text-white group-hover:text-white dark:group-hover:text-black transition-all duration-300 delay-100'>
        {children}
      </span>
    </button>
  )
}

/**
 * # FlatButton
 *
 * the button with flat design
 * and become the relative version of awesome button
 * nice to combine
 *
 * @returns JSX.Element
 */
const FlatButton: FunctionComponent<FlatButtonProps> = ({
  children,
  disable,
  onClick,
  form,
  className,
  type = 'button',
}): JSX.Element => {
  return (
    <button
      onClick={onClick}
      disabled={disable}
      type={type ?? form ? 'submit' : 'button'}
      form={form}
      className={`flex relative px-8 py-5 justify-center items-center rounded-full overflow-hidden text-button font-medium border border-gray-normal dark:border-gray-darker dark:border-opacity-60 group transition-all duration-300 hover:scale-95 ${className}`}
    >
      <span className='absolute h-full w-full rounded-full bg-black dark:bg-white transition-all duration-500 -translate-x-0 group-hover:translate-x-[110%] group-disabled:group-hover:-translate-x-full'></span>
      <span className='relative gap-4 flex w-full justify-center items-center text-white dark:text-black group-hover:text-black dark:group-hover:text-white transition-all duration-300 delay-100'>
        {children}
      </span>
    </button>
  )
}

export { AwesomeButton, FlatButton }
