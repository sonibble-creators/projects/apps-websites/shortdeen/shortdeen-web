import Link from 'next/link'
import { FunctionComponent, useEffect, useState } from 'react'

/**
 * # MainFooter
 *
 * the main footer that contain
 * the links and credits
 *
 * @returns JSX.Element
 */
const MainFooter: FunctionComponent = (): JSX.Element => {
  const [domain, setDomain] = useState<string>('')

  useEffect(() => {
    setDomain(window.location.host)
  }, [])

  return (
    <footer
      className='flex py-10 border-t border-t-gray-light dark:border-gray-darker dark:border-opacity-30'
      id='main-footer'
    >
      <div className='flex container mx-auto desktop:px-0 px-5'>
        <div className='flex w-full justify-between'>
          <div className='flex items-center'>
            <span className='font-medium'>
              Copyright &copy;{' '}
              <span data-cursor-exclusion data-cursor-size='80'>
                {domain}
              </span>
            </span>
          </div>
          <div className='flex'>
            <ul className='flex list-none'>
              <li className='flex'>
                <Link href={'/privacy'}>
                  <span
                    className='flex cursor-pointer font-medium'
                    data-cursor-exclusion
                    data-cursor-size='80'
                  >
                    Privacy
                  </span>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default MainFooter
