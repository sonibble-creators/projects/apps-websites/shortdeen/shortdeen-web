import {
  ChangeEvent,
  ChangeEventHandler,
  FunctionComponent,
  useMemo,
  useState,
} from 'react'
import ReactDatePicker from 'react-datepicker'
import { useFormContext } from 'react-hook-form'

interface BaseInputProps {
  className?: string
  name: string
  label?: string
  placeholder?: string
  initial?: any
  onChange?: (e: any) => void
  style?: 'border' | 'round'
  scale?: 'normal' | 'snug'
  readonly?: boolean
}

interface TextInputProps extends BaseInputProps {}

interface LongTextInputProps extends BaseInputProps {}

interface HiddenInputProps extends BaseInputProps {
  type?: 'file' | 'image' | 'text'
}

interface DateInputProps extends BaseInputProps {
  format?: string
  min?: Date
  max?: Date
}

/**
 * # TextInput
 *
 * input form that use to handle the text
 * type of input form the form
 *
 * @returns JSX.Element
 */
const TextInput: FunctionComponent<TextInputProps> = ({
  name,
  placeholder,
  label,
  initial,
  className,
  style = 'round',
  scale = 'normal',
  readonly,
}): JSX.Element => {
  const {
    register,
    formState: { errors },
    setValue,
  } = useFormContext()

  useMemo(() => {
    setValue(name, initial)
  }, [initial])

  return (
    <div className={`flex flex-col gap-3 ${className}`}>
      {label && (
        <label
          htmlFor={`${name}-input`}
          className='font-medium text-gray-dark ml-2 text-body-small'
        >
          {label}
        </label>
      )}
      <div
        className={`flex w-full ${
          style == 'border'
            ? 'border-b focus-within:border-b-2'
            : 'border rounded-2xl focus-within:border-2'
        } border-gray-normal dark:border-gray-dark dark:border-opacity-30 ${
          scale == 'snug' ? 'pb-6' : 'h-14'
        } focus-within:border-black dark:focus-within:border-gray-light transition-all duration-300 hover:-translate-y-1 cursor-auto px-5 justify-center items-center`}
      >
        <input
          type='text'
          {...register(name)}
          className={`flex w-full bg-transparent border-none outline-none focus:border-none focus:ring-0 ${
            scale == 'snug'
              ? 'placeholder:text-heading-5 font-normal placeholder:text-gray-dark text-heading-5 text-black dark:text-gray-light'
              : 'placeholder:text-body-small placeholder:text-gray-dark text-body-normal text-black dark:text-gray-light'
          }`}
          id={`${name}-input`}
          placeholder={placeholder}
          readOnly={readonly}
        />
      </div>
      {errors[name]?.message && (
        <span className='text-body-small text-red ml-3'>
          {errors[name]?.message?.toString()}
        </span>
      )}
    </div>
  )
}

/**
 * # PasswordInput
 *
 * the input form
 * for password field
 *
 * @returns JSX.Element
 */
const PasswordInput: FunctionComponent<TextInputProps> = ({
  name,
  placeholder,
  label,
  initial,
  className,
  style = 'round',
  scale = 'normal',
  readonly,
}): JSX.Element => {
  const {
    register,
    formState: { errors },
    setValue,
  } = useFormContext()
  const [isShowPassword, setIsShowPassword] = useState<boolean>(false)

  const toggleShowPassword: () => void = () => {
    setIsShowPassword(!isShowPassword)
  }

  useMemo(() => {
    setValue(name, initial)
  }, [initial])

  return (
    <div className={`flex flex-col gap-3 ${className}`}>
      {label && (
        <label
          htmlFor={`${name}-input`}
          className='font-medium text-gray-dark ml-2 text-body-small'
        >
          {label}
        </label>
      )}
      <div
        className={`flex w-full ${
          style == 'border'
            ? 'border-b focus-within:border-b-2'
            : 'border rounded-2xl focus-within:border-2'
        } border-gray-normal dark:border-gray-dark dark:border-opacity-30 ${
          scale == 'snug' ? 'pb-6' : 'h-14'
        } focus-within:border-black dark:focus-within:border-gray-light transition-all duration-300 hover:-translate-y-1 cursor-auto px-5 justify-center items-center`}
      >
        <input
          type={isShowPassword ? 'text' : 'password'}
          {...register(name)}
          className={`flex flex-1 bg-transparent border-none outline-none focus:border-none focus:ring-0 ${
            scale == 'snug'
              ? 'placeholder:text-heading-5 font-normal placeholder:text-gray-dark text-heading-5 text-black dark:text-gray-light'
              : 'placeholder:text-body-small placeholder:text-gray-dark text-body-normal text-black dark:text-gray-light'
          }`}
          id={`${name}-input`}
          placeholder={placeholder}
          readOnly={readonly}
        />
        <button
          onClick={toggleShowPassword}
          className='flex justify-center items-center'
          type='button'
        >
          <i
            className={`fi ${
              isShowPassword ? 'fi-rr-eye ' : 'fi-rr-eye-crossed'
            } flex justify-center items-center text-[20px] text-gray-dark`}
          ></i>
        </button>
      </div>
      {errors[name]?.message && (
        <span className='text-body-small text-red ml-3'>
          {errors[name]?.message?.toString()}
        </span>
      )}
    </div>
  )
}

/**
 * # LongTextInput
 *
 * the input for text area
 * that showing the long text input
 *
 * @returns JSX.Element
 */
const LongTextInput: FunctionComponent<LongTextInputProps> = ({
  name,
  placeholder,
  label,
  initial,
  className,
  style = 'round',
  scale = 'normal',
  readonly,
}): JSX.Element => {
  const {
    register,
    formState: { errors },
    setValue,
  } = useFormContext()

  useMemo(() => {
    setValue(name, initial)
  }, [initial])

  return (
    <div className={`flex flex-col gap-3 ${className}`}>
      {label && (
        <label
          htmlFor={`${name}-input`}
          className='font-medium text-gray-dark ml-2 text-body-small'
        >
          {label}
        </label>
      )}
      <div
        className={`flex w-full ${
          style == 'border'
            ? 'border-b focus-within:border-b-2'
            : 'border rounded-2xl focus-within:border-2'
        } border-gray-normal dark:border-gray-dark dark:border-opacity-30 ${
          scale == 'snug' ? 'pb-6' : 'h-[200px]'
        } focus-within:border-black dark:focus-within:border-gray-light transition-all duration-300 hover:-translate-y-1 cursor-auto px-5 justify-center items-center`}
      >
        <textarea
          {...register(name)}
          className={`flex w-full h-full bg-transparent border-none outline-none focus:border-none focus:ring-0 ${
            scale == 'snug'
              ? 'placeholder:text-heading-5 font-normal placeholder:text-gray-dark text-heading-5 text-black dark:text-gray-light'
              : 'placeholder:text-body-small placeholder:text-gray-dark text-body-normal text-black dark:text-gray-light'
          }`}
          id={`${name}-input`}
          placeholder={placeholder}
          readOnly={readonly}
        ></textarea>
      </div>
      {errors[name]?.message && (
        <span className='text-body-small text-red ml-3'>
          {errors[name]?.message?.toString()}
        </span>
      )}
    </div>
  )
}

/**
 * # HiddenInput
 *
 * the input for hidden text and
 * some value inputs
 *
 * @returns JSX.Element
 */
const HiddenInput: FunctionComponent<HiddenInputProps> = ({
  name,
  initial,
  type,
  onChange,
}): JSX.Element => {
  const { register, setValue } = useFormContext()

  const onChangeInput: (e: ChangeEvent<HTMLInputElement>) => void = (e) => {
    if (onChange && type == 'file') {
      onChange(e.target.files)
    }
  }

  useMemo(() => {
    setValue(name, initial)
  }, [initial])

  useMemo

  return (
    <input
      type={type}
      className='hidden sr-only'
      {...register(name)}
      id={`${name}-input`}
      onInput={onChangeInput}
    />
  )
}

const getRangeYear: (start: number, length: number) => number[] = (
  start,
  length
) => {
  let years: number[] = []
  years = Array(start - (start - length))
    .fill('')
    .map((year, position) => start - position) as number[]
  return years
}

const DateInput: FunctionComponent<DateInputProps> = ({
  name,
  placeholder,
  label,
  initial,
  className,
  style = 'round',
  scale = 'normal',
  format = 'EEEE, dd MMMM yyyy',
  min,
  max,
}): JSX.Element => {
  const {
    register,
    formState: { errors },
    setValue,
  } = useFormContext()
  const [selectedDate, setSelectedDate] = useState<Date>()
  const years = getRangeYear(new Date().getFullYear(), 100)
  const months: string[] = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ]

  const onPickedDateChange: (date: Date) => void = (date) => {
    setSelectedDate(date)
    let isoDate = date.toISOString()
    setValue(name, isoDate)
  }

  useMemo(() => {
    setValue(name, initial)
  }, [initial])

  return (
    <div className={`flex flex-col gap-3 ${className}`}>
      {label && (
        <label
          htmlFor={`${name}-input`}
          className='font-medium text-gray-dark ml-2 text-body-small'
        >
          {label}
        </label>
      )}
      <div
        className={`flex w-full ${
          style == 'border'
            ? 'border-b focus-within:border-b-2'
            : 'border rounded-2xl focus-within:border-2'
        } border-gray-normal dark:border-gray-dark dark:border-opacity-30 ${
          scale == 'snug' ? 'pb-6' : 'h-14'
        } focus-within:border-black dark:focus-within:border-gray-light transition-all duration-300 hover:-translate-y-1 cursor-auto px-5 justify-center items-center`}
      >
        <input
          type='text'
          {...register(name)}
          className='hidden'
          id={`${name}-input`}
          readOnly
          placeholder={placeholder}
        />

        <ReactDatePicker
          renderCustomHeader={({
            date,
            changeYear,
            changeMonth,
            decreaseMonth,
            increaseMonth,
            prevMonthButtonDisabled,
            nextMonthButtonDisabled,
          }) => (
            <div className='flex w-full gap-4'>
              <div className='flex justify-center items-center'>
                <button
                  onClick={decreaseMonth}
                  disabled={prevMonthButtonDisabled}
                  className='flex justify-center items-center transition-all duration-200 hover:text-awesome-green hover:-translate-y-2'
                >
                  <i className='uil uil-angle-left-b text-xl'></i>
                </button>
              </div>
              <div className='flex flex-1 gap-3'>
                <select
                  value={date.getFullYear().toString()}
                  onChange={(e) => {
                    const year = parseInt(e.target.value, 10)
                    changeYear(year)
                  }}
                  className='flex border border-awesome-light-gray rounded-xl focus:border-awesome-green focus:ring-0 focus:ring-transparent'
                >
                  {years.map((option) => (
                    <option key={option} value={option}>
                      {option}
                    </option>
                  ))}
                </select>

                <select
                  value={months[date.getMonth()]}
                  onChange={(e) => {
                    const month = e.target.value
                    changeMonth(months.indexOf(month))
                  }}
                  className='flex border border-awesome-light-gray rounded-xl focus:border-awesome-green focus:ring-0 focus:ring-transparent'
                >
                  {months.map((option) => (
                    <option key={option} value={option}>
                      {option}
                    </option>
                  ))}
                </select>
              </div>
              <div className='flex justify-center items-center'>
                <button
                  onClick={increaseMonth}
                  disabled={nextMonthButtonDisabled}
                  className='flex justify-center items-center transition-all duration-200 hover:text-awesome-green hover:-translate-y-2'
                >
                  <i className='uil uil-angle-right-b text-xl'></i>
                </button>
              </div>
            </div>
          )}
          selected={selectedDate}
          onChange={onPickedDateChange}
          className='flex w-full flex-1 bg-transparent border-none outline-none focus:border-none focus:ring-0'
          dateFormat={format}
          popperClassName='bg-white'
          wrapperClassName='flex h-full w-full items-center'
          minDate={min}
          maxDate={max}
          placeholderText={placeholder}
        />
      </div>
      {errors[name]?.message && (
        <span className='text-body-small text-red ml-3'>
          {errors[name]?.message?.toString()}
        </span>
      )}
    </div>
  )
}

export { TextInput, LongTextInput, HiddenInput, PasswordInput, DateInput }
