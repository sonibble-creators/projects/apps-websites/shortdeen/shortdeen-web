# CHANGELOG.md

## 1.0.0 (2022-10-23)

Main Release:

- Complete simple and modern design
- Fully support dark mode
- Fully responsive support
- SEO Friendly
- First Shortened Url
