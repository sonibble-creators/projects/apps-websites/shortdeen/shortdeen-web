/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  eslint: {
    dirs: ['pages', 'components', 'libs'],
  },
  images: {
    domains: ['cdn.sanity.io'],
  },
}

module.exports = nextConfig
