import { FlatButton } from '@components/base/button'
import { Form } from '@components/base/form'
import {
  DateInput,
  HiddenInput,
  LongTextInput,
  TextInput,
} from '@components/base/input'
import SettingLayout from '@components/base/layout/setting'
import { useAlert } from '@hooks/alert'
import { isAuthenticated, withAuthServer } from '@hooks/auth'
import { NextPageWithlayout } from '@pages/_app'
import accountService from '@services/account'
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import { useState } from 'react'
import { z } from 'zod'

const updateProfileFormSchema = z.object({
  fullName: z.string().min(1, { message: 'Please add your name' }),
  email: z
    .string()
    .min(1, { message: 'Please fill the email address' })
    .email({ message: 'Please add a valid email address' }),
  username: z.string().min(1, { message: 'Please add the username' }),
  location: z.string().optional(),
  birthDate: z.string().optional(),
  bio: z.string().optional(),
  cover: z.any(),
  avatar: z.any(),
  profileId: z.string(),
})

const getServerSideProps: GetServerSideProps = withAuthServer(
  async (context) => {
    // validation and rules for the user
    const auth = await isAuthenticated(context)
    if (!auth) {
      return {
        redirect: {
          destination: '/',
          permanent: false,
        },
      }
    }
    return { props: {} }
  }
)

/**
 * # ProfileSettingPage
 *
 * the setting page for profile
 * handle the data and update setting about the profile
 *
 * @returns JSX.Element
 */
const ProfileSettingPage: NextPageWithlayout = (): JSX.Element => {
  const { data, isLoading } = useQuery(['account'], accountService.loadAccount)
  const [pickedAvatar, setPickedAvatar] = useState<string>()
  const [pickedCover, setPickedCover] = useState<string>()
  const alert = useAlert()
  const queryClient = useQueryClient()

  const updateProfile = useMutation(accountService.completeProfile, {
    onSuccess: () => {
      alert.show({
        type: 'success',
        title: 'Updated',
        description: 'Nice, the profile already updated',
      })
      queryClient.invalidateQueries(['account'])
    },
    onError: () => {
      alert.show({
        type: 'error',
        title: 'Failed',
        description: 'Opps, something error when try to update your profile',
      })
    },
  })

  const onFormSubmitted: (formData: any) => void = (formData) => {
    updateProfile.mutate(formData)
  }

  const onAvatarPicked = (e: any) => {
    const files = e as FileList
    if (files.length > 0) {
      const file = files[0]
      const url = URL.createObjectURL(file)
      setPickedAvatar(url)
    }
  }

  const onCoverPicked = (e: any) => {
    const files = e as FileList
    if (files.length > 0) {
      const file = files[0]
      const url = URL.createObjectURL(file)
      setPickedCover(url)
    }
  }

  return (
    <>
      <Head>
        <title>Update Profile - Shortdeen</title>
        <meta name='description' content='Update your current profile' />
      </Head>

      <div className='flex flex-col min-h-screen'>
        {!isLoading ? (
          <>
            <div className='flex'>
              <h3
                className='text-heading-4 desktop:text-heading-3 font-medium font-poppins leading-tight'
                data-aos='fade-up'
              >
                Profile.
              </h3>
            </div>
            <Form
              id='update-profile-form'
              mode='onChange'
              onSubmit={onFormSubmitted}
              resolverSchema={updateProfileFormSchema}
              className='mt-20'
            >
              {/* hidden input */}
              <HiddenInput name='profileId' initial={data?.profile?.id} />
              <HiddenInput
                name='avatar'
                type='file'
                onChange={onAvatarPicked}
              />
              <HiddenInput name='cover' type='file' onChange={onCoverPicked} />

              <div
                className='flex relative justify-center'
                data-aos='fade-up'
                data-aos-delay='200'
              >
                <div className='flex w-full'>
                  <div className='flex relative w-full h-[340px] overflow-hidden rounded-3xl border border-gray-light dark:border-gray-dark dark:border-opacity-50'>
                    <Image
                      src={
                        pickedCover ??
                        data?.profile?.cover ??
                        '/assets/image/cover-placeholder.png'
                      }
                      layout='fill'
                      objectFit='cover'
                      alt='Cover image'
                    />
                  </div>
                  <label
                    htmlFor='cover-input'
                    className=' flex justify-center items-center h-10 w-10 bg-black dark:bg-white text-white dark:text-black absolute top-4 right-4 rounded-full transition-all duration-500 hover:-translate-y-1 hover:scale-105 cursor-pointer'
                  >
                    <i className='fi fi-rr-camera flex justify-center items-center text-[20px]'></i>
                  </label>
                </div>

                <div className='flex p-2 bg-white dark:bg-black  rounded-full h-28 w-28 absolute -bottom-14'>
                  <div className='flex relative overflow-hidden rounded-full h-full w-full'>
                    <Image
                      src={
                        pickedAvatar ??
                        data?.profile?.avatar ??
                        '/assets/image/avatar-placeholder.png'
                      }
                      layout='fill'
                      objectFit='cover'
                      alt='Cover image'
                    />
                  </div>
                  <label
                    htmlFor='avatar-input'
                    className='flex justify-center items-center h-10 w-10 bg-black text-white dark:bg-white dark:text-black absolute bottom-0 right-0 rounded-full transition-all duration-500 hover:-translate-y-1 hover:scale-105 cursor-pointer'
                  >
                    <i className='fi fi-rr-camera flex justify-center items-center text-[20px]'></i>
                  </label>
                </div>
              </div>

              <div
                className='flex flex-col mt-relaxed'
                data-aos='fade-up'
                data-aos-delay='400'
              >
                <div className='flex'>
                  <h4 className='text-heading-4 font-medium font-poppins'>
                    Basic
                  </h4>
                </div>
                <div className='flex flex-col mt-14 gap-6'>
                  <TextInput
                    name='fullName'
                    placeholder='Your full name'
                    label='Full Name'
                    initial={data?.profile?.fullName}
                  />
                  <TextInput
                    name='email'
                    placeholder='Your email address'
                    label='Email'
                    initial={data?.profile?.email}
                  />
                  <TextInput
                    name='username'
                    placeholder='Your username'
                    label='Username'
                    initial={data?.profile?.username}
                  />
                </div>
              </div>

              <div
                className='flex flex-col mt-20'
                data-aos='fade-up'
                data-aos-delay='200'
              >
                <div className='flex'>
                  <h4 className='text-heading-4 font-medium font-poppins'>
                    Detail
                  </h4>
                </div>
                <div className='flex flex-col mt-14 gap-6'>
                  <TextInput
                    name='location'
                    placeholder='Your location address'
                    label='Location'
                    initial={data?.profile?.location}
                  />
                  <DateInput
                    name='birthDate'
                    placeholder='Your birth date'
                    label='Birth Date'
                    initial={data?.profile?.birthDate}
                  />
                  <LongTextInput
                    name='bio'
                    placeholder='Describe about yourself'
                    label='Bio'
                    initial={data?.profile?.bio}
                  />
                </div>
              </div>
            </Form>
            <div
              className='flex mt-relaxed'
              data-aos='fade-up'
              data-aos-delay='200'
            >
              <FlatButton
                disable={updateProfile.isLoading}
                form='update-profile-form'
                className='w-full'
                type='submit'
              >
                {updateProfile.isLoading ? (
                  <>
                    Updating profile
                    <i className='fi fi-rr-spinner flex justify-center items-center text-[24px] absolute right-0 animate-spin' />
                  </>
                ) : (
                  <>
                    Save
                    <i className='fi fi-rr-arrow-right flex justify-center items-center text-[24px] absolute right-0' />
                  </>
                )}
              </FlatButton>
            </div>
          </>
        ) : (
          <div className='flex flex-col'>
            <div className='flex h-16 bg-gray-normal rounded-2xl w-4/12 animate-skeleton'></div>

            <div className='flex mt-24 w-full h-[340px] bg-gray-light animate-skeleton rounded-3xl'></div>

            <div className='flex flex-col mt-28'>
              <div className='flex h-16 bg-gray-normal rounded-2xl w-3/12 animate-skeleton'></div>

              <div className='flex flex-col gap-6 mt-24'>
                <div className='flex w-full h-14 bg-gray-light animate-skeleton rounded-2xl'></div>
                <div className='flex w-full h-14 bg-gray-light animate-skeleton rounded-2xl'></div>
                <div className='flex w-full h-14 bg-gray-light animate-skeleton rounded-2xl'></div>
              </div>
            </div>
            <div className='flex flex-col mt-28'>
              <div className='flex h-16 bg-gray-normal rounded-2xl w-3/12 animate-skeleton'></div>

              <div className='flex flex-col gap-6 mt-24'>
                <div className='flex w-full h-14 bg-gray-light animate-skeleton rounded-2xl'></div>
                <div className='flex w-full h-14 bg-gray-light animate-skeleton rounded-2xl'></div>
                <div className='flex w-full h-14 bg-gray-light animate-skeleton rounded-2xl'></div>
              </div>
            </div>
            <div className='flex h-16 mt-28 bg-gray-normal rounded-full animate-skeleton'></div>
          </div>
        )}
      </div>
    </>
  )
}

ProfileSettingPage.layout = (page) => {
  return <SettingLayout>{page}</SettingLayout>
}

export { getServerSideProps }
export default ProfileSettingPage
