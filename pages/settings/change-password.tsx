import { FlatButton } from '@components/base/button'
import { Form } from '@components/base/form'
import { PasswordInput, TextInput } from '@components/base/input'
import SettingLayout from '@components/base/layout/setting'
import { zodResolver } from '@hookform/resolvers/zod'
import { useAlert } from '@hooks/alert'
import { isAuthenticated, withAuthServer } from '@hooks/auth'
import { AwesomeError } from '@models/error'
import { NextPageWithlayout } from '@pages/_app'
import accountService from '@services/account'
import { useMutation } from '@tanstack/react-query'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import { useForm } from 'react-hook-form'
import { z } from 'zod'

const changePasswordFormSchema = z.object({
  currentPassword: z
    .string()
    .min(1, { message: 'Please fill your current password' }),
  newPassword: z.string().min(1, { message: 'Please fill the new password' }),
  confirmPassword: z
    .string()
    .min(1, { message: 'Please fill the confirm password' }),
})

const getServerSideProps: GetServerSideProps = withAuthServer(
  async (context) => {
    // validation and rules for the user
    const auth = await isAuthenticated(context)
    if (!auth) {
      return {
        redirect: {
          destination: '/',
          permanent: false,
        },
      }
    }
    return { props: {} }
  }
)

/**
 * # ChangePasswordPage
 *
 * the setting page for handle
 * the change and update the password of the user
 *
 * @returns JSX.Element
 */
const ChangePasswordPage: NextPageWithlayout = (): JSX.Element => {
  const alert = useAlert()
  const changePasswordForm = useForm({
    mode: 'onChange',
    resolver: zodResolver(changePasswordFormSchema),
  })

  const changePassword = useMutation<void, AwesomeError>(
    accountService.changePassword,
    {
      onSuccess: () => {
        changePasswordForm.reset()
        alert.show({
          type: 'success',
          title: 'Updated',
          description: 'Nice, the password already updated',
        })
      },
      onError: (error, variables, context) => {
        changePasswordForm.reset()
        if (error.code == 'change-password/invalid-password') {
          changePasswordForm.setError('currentPassword', {
            message: error.message,
          })
        } else if (error.code == 'change-password/new-password-not-match') {
          changePasswordForm.setError('confirmPassword', {
            message: error.message,
          })
        } else {
          alert.show({
            type: 'error',
            title: 'Failed',
            description: error.message,
          })
        }
      },
    }
  )

  const onFormSubmitted: (formData: any) => void = (formData) => {
    changePassword.mutate(formData)
  }

  return (
    <>
      <Head>
        <title>Change Password - Shortdeen</title>
        <meta
          name='description'
          content='Change the password with the new one'
        />
      </Head>

      <div className='flex flex-col min-h-screen'>
        <div className='flex'>
          <h3
            className='text-heading-4 desktop:text-heading-3 font-medium font-poppins leading-tight'
            data-aos='fade-up'
          >
            Change password.
          </h3>
        </div>
        <Form
          id='change-password-form'
          context={changePasswordForm}
          onSubmit={onFormSubmitted}
          className='mt-20'
        >
          <div
            className='flex flex-col gap-6'
            data-aos='fade-up'
            data-aos-delay='200'
          >
            <TextInput
              name='currentPassword'
              placeholder='Your current password'
              label='Current password'
            />
            <TextInput
              name='newPassword'
              placeholder='Your new password'
              label='New Password'
            />
            <PasswordInput
              name='confirmPassword'
              placeholder='Retype your password'
              label='Confirm password'
            />
          </div>
        </Form>
        <div
          className='flex mt-relaxed'
          data-aos='fade-up'
          data-aos-delay='400'
        >
          <FlatButton
            disable={changePassword.isLoading}
            form='change-password-form'
            className='w-full'
            type='submit'
          >
            {changePassword.isLoading ? (
              <>
                Updating password
                <i className='fi fi-rr-spinner flex justify-center items-center text-[24px] absolute right-0 animate-spin' />
              </>
            ) : (
              <>
                Change Password
                <i className='fi fi-rr-arrow-right flex justify-center items-center text-[24px] absolute right-0' />
              </>
            )}
          </FlatButton>
        </div>
      </div>
    </>
  )
}

ChangePasswordPage.layout = (page) => {
  return <SettingLayout>{page}</SettingLayout>
}

export { getServerSideProps }
export default ChangePasswordPage
