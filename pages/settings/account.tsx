import { FlatButton } from '@components/base/button'
import SettingLayout from '@components/base/layout/setting'
import { useAlert } from '@hooks/alert'
import { isAuthenticated, withAuthServer } from '@hooks/auth'
import { AwesomeError } from '@models/error'
import { NextPageWithlayout } from '@pages/_app'
import accountService from '@services/account'
import { useMutation } from '@tanstack/react-query'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import { useRouter } from 'next/router'

const getServerSideProps: GetServerSideProps = withAuthServer(
  async (context) => {
    // validation and rules for the user
    const auth = await isAuthenticated(context)
    if (!auth) {
      return {
        redirect: {
          destination: '/',
          permanent: false,
        },
      }
    }
    return { props: {} }
  }
)

/**
 * # AccountSettingPage
 *
 * the setting page for handle
 * the account setting including to delete the current account
 *
 * @returns JSX.Element
 */
const AccountSettingPage: NextPageWithlayout = (): JSX.Element => {
  const alert = useAlert()
  const router = useRouter()

  const deleteAccount = useMutation<void, AwesomeError>(
    accountService.deleteAccount,
    {
      onSuccess: () => {
        alert.show({
          type: 'success',
          title: 'Updated',
          description: 'Account already deleted',
        })

        router.push('/')
      },
      onError: (error, variables, context) => {
        alert.show({
          type: 'error',
          title: 'Failed',
          description: error.message,
        })
      },
    }
  )

  const onDeleteAccount: () => void = () => {
    deleteAccount.mutate()
  }

  return (
    <>
      <Head>
        <title>Accounts - Shortdeen</title>
        <meta name='description' content='Manage your accounts' />
      </Head>

      <div className='flex flex-col min-h-screen'>
        <div className='flex'>
          <h3
            className='text-heading-4 desktop:text-heading-3 font-medium font-poppins leading-tight'
            data-aos='fade-up'
          >
            Accounts.
          </h3>
        </div>

        <div className='flex flex-col mt-20 gap-6'>
          <h4
            className='text-heading-5 desktop:text-heading-4 font-medium font-poppins'
            data-aos='fade-up'
            data-aos-delay='200'
          >
            Delete account
          </h4>
          <span className='' data-aos='fade-up' data-aos-delay='400'>
            Delete your account will cause delete all of the link that used by
            signup using this account.
          </span>
        </div>
        <div className='flex mt-20' data-aos='fade-up' data-aos-delay='600'>
          <FlatButton
            disable={deleteAccount.isLoading}
            className='w-full'
            type='button'
            onClick={onDeleteAccount}
          >
            {deleteAccount.isLoading ? (
              <>
                Deleting Account
                <i className='fi fi-rr-spinner flex justify-center items-center text-[24px] absolute right-0 animate-spin' />
              </>
            ) : (
              <>
                Delete Account
                <i className='fi fi-rr-arrow-right flex justify-center items-center text-[24px] absolute right-0' />
              </>
            )}
          </FlatButton>
        </div>
      </div>
    </>
  )
}

AccountSettingPage.layout = (page) => {
  return <SettingLayout>{page}</SettingLayout>
}

export { getServerSideProps }
export default AccountSettingPage
