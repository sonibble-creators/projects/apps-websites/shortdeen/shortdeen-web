import { NextPage } from 'next'
import type { AppProps } from 'next/app'
import { ReactElement, ReactNode, useEffect, useState } from 'react'
import '@styles/main.css'
import AwesomeCursor from 'react-awesome-cursor'
import 'react-awesome-cursor/dist/main.css'
import {
  QueryClient,
  QueryClientProvider,
  Hydrate,
} from '@tanstack/react-query'
import { ReactQueryDevtools } from '@tanstack/react-query-devtools'
import AOS from 'aos'
import 'aos/dist/aos.css'
import AlertModal from '@components/modal/alert'
import { checkThemeDevice, checkThemeMode } from '@hooks/theme'
import SignInModal from '@components/modal/signin'
import SignOutModal from '@components/modal/signout'

export type NextPageWithlayout<P = {}, IP = P> = NextPage<P, IP> & {
  layout?: (page: ReactElement) => ReactNode
}

type AppPropsWithLayout = AppProps<any> & {
  Component: NextPageWithlayout
}

/**
 * # BisoolApp
 *
 * the app that contain all about the pages
 * component and props
 *
 * @params data passed to the component
 * @returns JSX.Element | ReactNode | ReactElement
 */
function BisoolApp({
  Component,
  pageProps,
}: AppPropsWithLayout): JSX.Element | ReactNode | ReactElement {
  const [queryClient] = useState(() => new QueryClient())
  const [mode, setMode] = useState<'light' | 'dark'>('light')
  const [device, setDevice] = useState<'mobile' | 'desktop' | 'tablet'>(
    'mobile'
  )

  // get the current layout in each pages
  const layout = Component.layout ?? ((page) => page)

  // init some config in top level
  useEffect(() => {
    // Animate on scroll library
    // use to add some animation to element when scrolling down
    AOS.init({
      once: true,
      duration: 1200,
      delay: 200,
      easing: 'ease-in-out',
    })
  }, [])

  // check the current device, and theme mode
  // in top level
  useEffect(() => {
    checkThemeMode((mode) => {
      setMode(mode)
    })
    checkThemeDevice((device) => {
      setDevice(device)
    })
  }, [])

  return (
    <QueryClientProvider client={queryClient}>
      <Hydrate state={pageProps.dehydratedState}>
        {layout(<Component {...pageProps} />)}
        {/* add the top level component */}
        <AlertModal />
        <SignInModal />
        <SignOutModal />
        {device != 'mobile' && (
          <AwesomeCursor backgroundColor={mode == 'dark' ? '#fff' : '#000'} />
        )}
      </Hydrate>
      <ReactQueryDevtools />
    </QueryClientProvider>
  )
}

export default BisoolApp
