import { AwesomeButton } from '@components/base/button'
import { AwesomeChip } from '@components/base/chip'
import { Form } from '@components/base/form'
import { HiddenInput, LongTextInput, TextInput } from '@components/base/input'
import GeneralLayout from '@components/base/layout/general'
import { zodResolver } from '@hookform/resolvers/zod'
import { useAlert } from '@libs/hooks/alert'
import contactService from '@services/contact'
import { useMutation } from '@tanstack/react-query'
import Head from 'next/head'
import { useState } from 'react'
import { useForm } from 'react-hook-form'
import { z } from 'zod'
import { NextPageWithlayout } from './_app'

const contactInterestList: string[] = [
  'Supports',
  'Ideas',
  'Contributions',
  'Help',
  'Suggestion',
]

const contactFormSchema = z.object({
  fullName: z.string().min(1, { message: 'Please fill your name' }),
  email: z.string().email({ message: 'Please provide the valid email' }),
  message: z.string().min(1, { message: 'Please specify your message' }),
  interests: z.string(),
})

/**
 * # ContactPage
 *
 * Page to handle the contact info
 * showing some info about the contact
 *
 * @returns JSX.Element
 */
const ContactPage: NextPageWithlayout = (): JSX.Element => {
  const [selectedInterest, setSelectedInterest] = useState<string[]>([])
  const contactForm = useForm({
    resolver: zodResolver(contactFormSchema),
    mode: 'onChange',
  })
  const alert = useAlert()

  const onInterestClicked: (interest: string) => void = (interest) => {
    const data = selectedInterest.slice() ?? []

    if (data.includes(interest)) {
      const index = data.indexOf(interest)
      data.splice(index, 1)
      setSelectedInterest(data)
    } else {
      data.push(interest)
      setSelectedInterest(data)
    }
  }

  const sendContactMessage = useMutation(contactService.sendMessage, {
    onSuccess: (data, variables, context) => {
      alert.show({
        type: 'success',
        title: 'Success',
        description: 'The message was sended',
      })
      contactForm.reset()
    },
    onError: (error, variables, context) => {
      alert.show({ type: 'error', title: 'Opps', description: error as string })
      contactForm.reset()
    },
  })

  const onFormSubmited: (formData: any) => void = async (formData) => {
    sendContactMessage.mutate({
      email: formData.email,
      fullName: formData.fullName,
      interests: formData.interests,
      message: formData.message,
    })
  }

  return (
    <>
      <Head>
        <title>Contact - Shortdeen</title>
        <meta
          name='description'
          content='Get in touch with us and build relations'
        />
      </Head>

      <main className='flex flex-col py-48 desktop:py-64'>
        <section className='flex container mx-auto flex-col w-full desktop:w-8/12 px-5 tablet:px-0'>
          <div className='flex w-full tablet:w-8/12'>
            <h1
              className='flex text-heading-3 desktop:text-heading-1 font-medium font-poppins leading-tight'
              data-cursor-size='180'
              data-cursor-exclusion
              data-aos='fade-up'
            >
              Tell us about everthing you want
            </h1>
          </div>
          <div className='flex flex-col mt-24 tablet:mt-relaxed gap-10'>
            <span
              className='flex text-heading-5 text-gray-dark font-poppins leading-normal'
              data-aos='fade-up'
              data-aos-delay='200'
            >
              You’re interested in ...
            </span>
            <div
              className='flex flex-wrap gap-x-6 gap-y-5 w-10/12'
              data-aos='fade-up'
              data-aos-delay='600'
            >
              {contactInterestList.map((interest, position) => (
                <AwesomeChip
                  onClick={() => {
                    onInterestClicked(interest)
                  }}
                  key={position}
                  isSelected={selectedInterest.includes(interest)}
                >
                  {interest}
                </AwesomeChip>
              ))}
            </div>
          </div>
          <div
            className='flex mt-20 tablet:mt-40 desktop:mt-snug flex-col gap-relaxed'
            data-aos='fade-up'
            data-aos-delay='800'
          >
            <Form
              onSubmit={onFormSubmited}
              className='!gap-20'
              id='contact-form'
              context={contactForm}
            >
              <HiddenInput
                name='interests'
                initial={selectedInterest.join(', ')}
              />
              <TextInput
                name='fullName'
                placeholder='Your full name'
                style='border'
                scale='snug'
              />
              <TextInput
                name='email'
                placeholder='Your email address'
                style='border'
                scale='snug'
              />
              <LongTextInput
                name='message'
                placeholder='Your messages'
                style='border'
                scale='snug'
              />
            </Form>

            <div
              className='flex justify-end'
              data-aos='fade-up'
              data-aos-delay='200'
            >
              <AwesomeButton
                form='contact-form'
                disable={sendContactMessage.isLoading}
              >
                {sendContactMessage.isLoading ? (
                  <>
                    Sending Messages
                    <i className='fi fi-rr-spinner flex justify-center items-center text-[24px] animate-spin'></i>
                  </>
                ) : (
                  <>
                    Send Message
                    <i className='fi fi-rr-paper-plane flex justify-center items-center text-[24px]'></i>
                  </>
                )}
              </AwesomeButton>
            </div>
          </div>
        </section>
      </main>
    </>
  )
}

ContactPage.layout = (page) => {
  return <GeneralLayout>{page}</GeneralLayout>
}

export default ContactPage
