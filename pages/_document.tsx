import { FunctionComponent } from 'react'
import { Head, Html, Main, NextScript } from 'next/document'

/**
 * # ShortdeenDocument
 *
 * the custom document for bisool
 * contain some configuration and import
 *
 * @returns JSX.Element
 */
const ShortdeenDocument: FunctionComponent = (): JSX.Element => {
  return (
    <Html>
      <Head>
        {/* meta tags */}
        <meta httpEquiv='X-UA-Compatible' content='IE=7' />
        <meta httpEquiv='X-UA-Compatible' content='ie=edge' />
        <link rel='shortcut icon' href='/favicon.png' type='image/x-icon' />

        {/* import some source from  */}
        <link rel='preconnect' href='https://fonts.googleapis.com' />
        <link
          rel='preconnect'
          href='https://fonts.gstatic.com'
          crossOrigin=''
        />
        <link
          href='https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;1,400;1,500&family=Poppins:ital,wght@0,400;0,500;1,400;1,500&display=swap'
          rel='stylesheet'
        />
        <link
          rel='stylesheet'
          href='https://cdn-uicons.flaticon.com/uicons-regular-rounded/css/uicons-regular-rounded.css'
        />
      </Head>
      <body className='text-body-normal leading-normal font-sans font-normal text-black bg-white dark:text-gray-light dark:bg-black no-scrollbar selection:text-gray-light selection:bg-black dark:selection:text-black dark:selection:bg-gray-light'>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}

export default ShortdeenDocument
