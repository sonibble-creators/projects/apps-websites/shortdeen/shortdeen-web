import { AwesomeButton, FlatButton } from '@components/base/button'
import { AwesomeChip } from '@components/base/chip'
import DashboardLayout from '@components/base/layout/dashboard'
import { useAlert } from '@hooks/alert'
import { isAuthenticated, withAuthServer } from '@hooks/auth'
import { AwesomeError } from '@models/error'
import { NextPageWithlayout } from '@pages/_app'
import linkService from '@services/link'
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { useEffect, useRef, useState } from 'react'

const getServerSideProps: GetServerSideProps = withAuthServer(
  async (context) => {
    const auth = await isAuthenticated(context)
    if (!auth) {
      return {
        redirect: {
          destination: '/',
          permanent: false,
        },
      }
    }
    return {
      props: {},
    }
  }
)

/**
 * # DashboardLinksPage
 *
 * the page to showing some info about summary
 * that include with the links and profile status
 *
 * @returns JSX.Element
 */
const DashboardLinksPage: NextPageWithlayout = (): JSX.Element => {
  const router = useRouter()
  const [domain, setDomain] = useState<string>('')
  const [page, setPage] = useState<number>(0)
  const [query, setQuery] = useState<string>()
  const orders: string[] = ['All', 'Oldest', 'Most Views']
  const [selectedOrder, setSelectedOrder] = useState<string>(orders[0])
  const links = useQuery(['links', page, selectedOrder, query], () =>
    linkService.loadLinks(page, selectedOrder, query)
  )
  const alert = useAlert()
  const queryClient = useQueryClient()

  const createShortenLink: () => void = () => {
    router.push('/')
  }
  const backPage: () => void = () => {
    if (page > 0) {
      setPage((currentPage) => currentPage - 1)
    }
  }

  const nextPage: () => void = () => {
    setPage((currentPage) => currentPage + 1)
    console.log(page)
  }

  const changeOrder: (order: string) => void = (order) => {
    setSelectedOrder(order)
  }

  const deleteLink = useMutation(linkService.deleteLink, {
    onSuccess: () => {
      alert.show({
        type: 'success',
        title: 'Deleted',
        description: 'the link already deleted',
      })
      queryClient.invalidateQueries(['links', page, selectedOrder, query])
    },
    onError: (error: AwesomeError, variables, context) => {
      alert.show({
        type: 'error',
        title: 'Failed',
        description: error.message,
      })
    },
  })

  const onDeleteLink: (id: string) => void = (id) => {
    deleteLink.mutate(id)
  }

  useEffect(() => {
    document
      .getElementById('search-input-link')
      ?.addEventListener('keypress', (e: any) => {
        if (e.code == 'Enter') {
          const searchQuery = e.target.value
          setQuery(searchQuery)
        }
      })
  })

  useEffect(() => {
    setDomain(window.location.host)
  }, [])

  return (
    <>
      <Head>
        <title>Links - Dashboard</title>
        <meta name='description' content='See all of the links that shorteen' />
      </Head>

      <div className='flex flex-col'>
        <div className='flex'>
          <h3 className='text-heading-4 desktop:text-heading-3 font-medium font-poppins'>
            Links.
          </h3>
        </div>

        <div className='flex mt-16 flex-col'>
          <div className='flex items-center gap-4'>
            {orders.map((order, position) => (
              <AwesomeChip
                isSelected={order == selectedOrder}
                onClick={() => changeOrder(order)}
                key={position}
              >
                {order}
              </AwesomeChip>
            ))}

            <div className='flex flex-1'></div>
            <div className='flex h-16 w-4/12 rounded-[20px] border border-gray-light px-5 items-center transition-all duration-300 focus-within:border-2 focus-within:border-black dark:focus-within:border-gray-light'>
              <input
                type='text'
                id='search-input-link'
                className='h-full flex flex-1 bg-transparent outline-none border-none ring-0 focus:outline-none focus:border-none focus:ring-0 placeholder:text-gray-darker'
                placeholder='Search your link...'
                defaultValue={query}
              />
              <i className='fi fi-rr-search flex justify-center items-center text-[24px] text-gray-darker'></i>
            </div>
          </div>
          {links.isSuccess ? (
            links.data.links.length > 0 ? (
              <div className='flex mt-10'>
                <table className='flex flex-col w-full'>
                  <thead className='hidden desktop:flex font-medium border-b border-b-gray-light dark:border-b-gray-dark dark:border-opacity-40 py-4'>
                    <tr className='flex w-full px-4'>
                      <th className='flex w-10'>No</th>
                      <th className='flex w-4/12'>Shorten Link</th>
                      <th className='flex w-5/12'>Source Link</th>
                      <th className='flex w-2/12'>Total Views</th>
                      <th className='flex w-1/12'>Actions</th>
                    </tr>
                  </thead>
                  <tbody className='flex flex-col mt-4 gap-1'>
                    {links.data.links.map(
                      ({ id, shortLink, totalView, sourceLink }, position) => (
                        <tr
                          className='flex flex-col desktop:flex-row w-full gap-4 border-b py-3 px-4 border-b-gray-light dark:border-b-gray-dark dark:border-opacity-40 transition-all duration-300 hover:-translate-y-1 hover:bg-gray-light cursor-pointer'
                          key={position}
                        >
                          <td className='hidden desktop:flex w-10 font-medium'>
                            {position + 1}
                          </td>
                          <td className='flex w-4/12 whitespace-nowrap overflow-hidden'>
                            {`${domain}/${shortLink}`}
                          </td>
                          <td className='flex w-5/12 whitespace-nowrap overflow-hidden'>
                            {sourceLink}
                          </td>
                          <td className='flex w-2/12 font-medium'>
                            {totalView ?? 0}
                          </td>
                          <td className='flex w-1/12 font-medium justify-end'>
                            <button className='flex h-8 w-8 justify-center items-center transition-all duration-300 hover:bg-black hover:text-white rounded-xl dark:hover:text-white group relative z-50'>
                              <i className='flex fi fi-rr-menu-dots'></i>

                              <div className='flex flex-col absolute -left-10 top-6 border-gray-normal bg-white rounded-2xl px-5 py-4 opacity-0 translate-x-full group-hover:translate-x-0 group-hover:opacity-100 shadow-md shadow-gray-light'>
                                <button
                                  onClick={() => onDeleteLink(id)}
                                  className='flex text-body-small font-medium text-black px-4 py-3 transition-all duration-300'
                                >
                                  Delete
                                </button>
                              </div>
                            </button>
                          </td>
                        </tr>
                      )
                    )}
                  </tbody>
                </table>
              </div>
            ) : (
              <div className='flex flex-col mt-16 items-center'>
                <div className='flex relative w-full desktop:w-7/12 h-[240px] desktop:h-[400px]'>
                  <Image
                    src='/assets/image/empty.svg'
                    layout='fill'
                    objectFit='contain'
                    alt='Not Found'
                  />
                </div>
                <h4 className='text-heading-5 desktop:text-heading-4 font-medium font-poppins mt-10'>
                  Not found
                </h4>
                <span className='mt-2 desktop:w-5/12 text-center'>
                  Opps, the link not found, Start shorting your link and enjoy
                  the simplicity
                </span>

                <div className='flex mt-16'>
                  <FlatButton onClick={createShortenLink} type='button'>
                    Shorten link
                    <i className='fi fi-rr-arrow-right flex justify-center items-center text-[24px]'></i>
                  </FlatButton>
                </div>
              </div>
            )
          ) : (
            <div className='flex flex-col gap-4 mt-16'>
              {[1, 2, 3, 4, 5].map((position) => (
                <div
                  className='flex w-full justify-between items-center gap-10'
                  key={position}
                >
                  <div className='flex h-8 w-8 rounded-2xl bg-gray-normal dark:bg-gray-dark dark:bg-opacity-60'></div>
                  <div className='flex h-4 flex-1 rounded-2xl bg-gray-light dark:bg-gray-dark dark:bg-opacity-60'></div>
                  <div className='flex h-4 w-3/12 rounded-2xl bg-gray-normal dark:bg-gray-dark dark:bg-opacity-60'></div>
                </div>
              ))}
            </div>
          )}
        </div>

        <div className='flex mt-32 justify-end items-center gap-8'>
          {page > 0 && (
            <AwesomeButton onClick={backPage} type='button'>
              <i className='fi fi-rr-arrow-left flex justify-center items-center text-[24px]'></i>
              Back
            </AwesomeButton>
          )}
          {!links.isPreviousData && links.data?.hasMore && (
            <FlatButton onClick={nextPage} type='button'>
              Next
              <i className='fi fi-rr-arrow-right flex justify-center items-center text-[24px]'></i>
            </FlatButton>
          )}
        </div>
      </div>
    </>
  )
}

DashboardLinksPage.layout = (page) => {
  return <DashboardLayout>{page}</DashboardLayout>
}

export { getServerSideProps }
export default DashboardLinksPage
