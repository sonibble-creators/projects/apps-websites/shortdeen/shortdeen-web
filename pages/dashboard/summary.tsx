import { FlatButton } from '@components/base/button'
import DashboardLayout from '@components/base/layout/dashboard'
import { isAuthenticated, withAuthServer } from '@hooks/auth'
import { NextPageWithlayout } from '@pages/_app'
import linkService from '@services/link'
import { useQuery } from '@tanstack/react-query'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'

const getServerSideProps: GetServerSideProps = withAuthServer(
  async (context) => {
    const auth = await isAuthenticated(context)
    if (!auth) {
      return {
        redirect: {
          destination: '/',
          permanent: false,
        },
      }
    }
    return {
      props: {},
    }
  }
)

/**
 * # DashboardSummaryPage
 *
 * the page to showing some info about summary
 * that include with the links and profile status
 *
 * @returns JSX.Element
 */
const DashboardSummaryPage: NextPageWithlayout = (): JSX.Element => {
  const router = useRouter()
  const [domain, setDomain] = useState<string>('')
  const summary = useQuery(['link-summary'], linkService.loadSummary)
  const popularLinks = useQuery(['popular-link'], linkService.loadPopularLinks)

  const viewMoreLink: () => void = () => {
    router.push('/dashboard/links')
  }
  const createShortenLink: () => void = () => {
    router.push('/')
  }

  useEffect(() => {
    setDomain(window.location.host)
  }, [])

  return (
    <>
      <Head>
        <title>Summary - Dashboard</title>
        <meta name='description' content='See the links summary' />
      </Head>

      <div className='flex flex-col'>
        <div className='flex'>
          <h3 className='text-heading-4 desktop:text-heading-3 font-medium font-poppins'>
            Summary.
          </h3>
        </div>

        {summary.isSuccess ? (
          <div className='flex flex-col desktop:flex-row mt-10 gap-2 desktop:gap-8'>
            <div className='flex desktop:w-3/12'>
              <div className='flex px-5 py-4 rounded-2xl border border-gray-light dark:border-gray-dark dark:border-opacity-40 gap-4 w-full cursor-pointer transition-all duration-300 hover:-translate-y-1'>
                <div className='flex h-16 w-16 rounded-2xl bg-[#DAFFC9] items-center justify-center transition-all duration-300 hover:scale-105'>
                  <i className='fi fi-rr-link flex justify-center items-center text-[24px] text-primary'></i>
                </div>
                <div className='flex flex-col gap-4'>
                  <span className='text-heading-3 font-medium font-poppins'>
                    {summary.data?.totalLinks}
                  </span>
                  <span className='text-body-small text-gray-dark'>
                    Total links
                  </span>
                </div>
              </div>
            </div>
            <div className='flex desktop:w-3/12'>
              <div className='flex px-5 py-4 rounded-2xl border border-gray-light dark:border-gray-dark dark:border-opacity-40 gap-4 w-full cursor-pointer transition-all duration-300 hover:-translate-y-1'>
                <div className='flex h-16 w-16 rounded-2xl bg-[#D0C9FF] items-center justify-center transition-all duration-300 hover:scale-105'>
                  <i className='fi fi-rr-eye flex justify-center items-center text-[24px] text-[#5226FF]'></i>
                </div>
                <div className='flex flex-col gap-4'>
                  <span className='text-heading-3 font-medium font-poppins'>
                    {summary.data?.totalViews}
                  </span>
                  <span className='text-body-small text-gray-dark'>
                    Total views
                  </span>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className='flex w-full flex-col desktop:flex-row animate-skeleton gap-8'>
            <div className='flex h-28 desktop:w-3/12 bg-gray-normal dark:bg-gray-dark dark:bg-opacity-60 rounded-3xl'></div>
            <div className='flex h-28 desktop:w-3/12 bg-gray-normal dark:bg-gray-dark dark:bg-opacity-60 rounded-3xl'></div>
          </div>
        )}

        {popularLinks.isSuccess ? (
          <div className='flex mt-16 flex-col desktop:w-10/12'>
            <div className='flex justify-between items-center'>
              <h4 className='text-heading-5 desktop:text-heading-4 font-medium font-poppins'>
                Popular Links.
              </h4>
              <button
                onClick={viewMoreLink}
                className='flex gap-4 items-center text-button font-medium transition-all duration-300 hover:-translate-x-1'
                type='button'
                data-cursor-exclusion
                data-cursor-size='120'
              >
                View More
                <i className='fi fi-rr-arrow-right flex justify-center items-center text-[24px]'></i>
              </button>
            </div>
            {popularLinks.data.length > 0 ? (
              <div className='flex mt-10'>
                <table className='flex flex-col w-full'>
                  <thead className='hidden desktop:flex font-medium border-b border-b-gray-light dark:border-b-gray-dark dark:border-opacity-40 py-4'>
                    <tr className='flex w-full'>
                      <th className='flex w-12'>No</th>
                      <th className='flex flex-1'>Shorten Link</th>
                      <th className='flex w-2/12'>Total Views</th>
                    </tr>
                  </thead>
                  <tbody className='flex flex-col mt-4 gap-1'>
                    {popularLinks.data.map(
                      ({ shortLink, totalView }, position) => (
                        <tr
                          className='flex flex-col desktop:flex-row w-full border-b py-3 border-b-gray-light dark:border-b-gray-dark dark:border-opacity-40 px-3 transition-all duration-300 hover:bg-black dark:hover:bg-white cursor-pointer hover:text-white dark:hover:text-black group'
                          key={position}
                        >
                          <td className='hidden desktop:flex w-12 font-medium'>
                            {position + 1}
                          </td>
                          <td className='flex flex-1 text-gray-darker group-hover:text-gray-light dark:group-hover:text-black'>
                            {`${domain}/${shortLink}`}
                          </td>
                          <td className='flex w-2/12 font-medium'>
                            {totalView}
                          </td>
                        </tr>
                      )
                    )}
                  </tbody>
                </table>
              </div>
            ) : (
              <div className='flex flex-col mt-16 items-center'>
                <div className='flex relative w-full desktop:w-7/12 h-[240px] desktop:h-[400px]'>
                  <Image
                    src='/assets/image/empty.svg'
                    layout='fill'
                    objectFit='contain'
                    alt='Not Found'
                  />
                </div>
                <h4 className='text-heading-5 desktop:text-heading-4 font-medium font-poppins mt-10'>
                  Not found
                </h4>
                <span className='mt-2 desktop:w-5/12 text-center'>
                  Opps, the link not found, Start shorting your link and enjoy
                  the simplicity
                </span>

                <div className='flex mt-16'>
                  <FlatButton onClick={createShortenLink} type='button'>
                    Shorten link
                    <i className='fi fi-rr-arrow-right flex justify-center items-center text-[24px]'></i>
                  </FlatButton>
                </div>
              </div>
            )}
          </div>
        ) : (
          <div className='flex w-full flex-col animate-skeleton gap-10 mt-16'>
            <div className='flex w-full justify-between items-center'>
              <div className='flex h-16 w-3/12 rounded-2xl bg-gray-normal dark:bg-gray-dark dark:bg-opacity-60'></div>
              <div className='flex h-5 w-2/12 rounded-2xl bg-gray-normal dark:bg-gray-dark dark:bg-opacity-60'></div>
            </div>

            <div className='flex flex-col gap-4'>
              {[1, 2, 3, 4, 5].map((position) => (
                <div
                  className='flex w-full justify-between items-center gap-10'
                  key={position}
                >
                  <div className='flex h-8 w-8 rounded-2xl bg-gray-normal dark:bg-gray-dark dark:bg-opacity-60'></div>
                  <div className='flex h-4 flex-1 rounded-2xl bg-gray-light dark:bg-gray-dark dark:bg-opacity-60'></div>
                  <div className='flex h-4 w-3/12 rounded-2xl bg-gray-normal dark:bg-gray-dark dark:bg-opacity-60'></div>
                </div>
              ))}
            </div>
          </div>
        )}
      </div>
    </>
  )
}

DashboardSummaryPage.layout = (page) => {
  return <DashboardLayout>{page}</DashboardLayout>
}

export { getServerSideProps }
export default DashboardSummaryPage
