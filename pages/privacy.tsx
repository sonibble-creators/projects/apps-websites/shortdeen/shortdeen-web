import GeneralLayout from '@components/base/layout/general'
import Head from 'next/head'
import { NextPageWithlayout } from './_app'

/**
 * # PrivacyPage
 *
 * Page to handle the contact info
 * showing some info about the contact
 *
 * @returns JSX.Element
 */
const PrivacyPage: NextPageWithlayout = (): JSX.Element => {
  return (
    <>
      <Head>
        <title>Privacy Policy - Shortdeen</title>
        <meta name='description' content='Our privacy policy' />
      </Head>

      <main className='flex flex-col py-64'>
        <div className='flex flex-col container mx-auto w-full px-5 tablet:px-0 tablet:w-6/12'>
          <div className='flex'>
            <h1
              className='text-heading-3 dekstop:text-heading-1 leading-tight tablet:text-heading-1 font-poppins font-medium dark:text-gray-light'
              data-cursor-size='180'
              data-cursor-exclusion
              data-aos='fade-up'
            >
              Privacy Policy
            </h1>
          </div>
          <div
            className='flex flex-col mt-36 text-body-small tablet:text-body-large desktop:text-heading-6 font-sans leading-8 gap-10 dark:text-gray-normal'
            data-aos='fade-up'
            data-aos-delay='1200'
          >
            <div className='flex'>
              <p className=''>Effective date: October 01, 2022</p>
            </div>
            <div className='flex'>
              <p className=''>
                Sonibble ("us", "we", or "our", "team", “me”) operates the{' '}
                <a
                  href='https://shortdeen.vercel.app'
                  target='_blank'
                  rel='noreferrer'
                  data-cursor-size='120'
                  data-cursor-exclusion
                >
                  https://shortdeen.vercel.app
                </a>{' '}
                website (the "Service").
              </p>
            </div>
            <div className='flex'>
              <p className=''>
                This page informs you of our policies regarding the collection,
                use, and disclosure of personal data when you use our Service
                and the choices you have associated with that data.
              </p>
            </div>
            <div className='flex'>
              <p className=''>
                We use your data to provide and improve the Service. By using
                the Service, you agree to the collection and use of information
                in accordance with this policy. Unless otherwise defined in this
                Privacy Policy, terms used in this Privacy Policy have the same
                meanings as in our Terms and Conditions, accessible from{' '}
                <a
                  href='https://shortdeen.vercel.app/privacy'
                  target='_blank'
                  rel='noreferrer'
                  data-cursor-size='120'
                  data-cursor-exclusion
                >
                  https://shortdeen.vercel.app/privacy
                </a>{' '}
              </p>
            </div>
            <div className='flex flex-col gap-10'>
              <h3 className='text-heading-4 desktop:text-heading-3 leading-tight font-medium font-poppins my-10'>
                Information Collection And Use
              </h3>
              <p className=''>
                We collect several different types of information for various
                purposes to provide and improve our Service to you.
              </p>
              <div className='flex flex-col gap-10'>
                <h4 className='text-heading-4 leading-tight font-poppins font-medium'>
                  Types of Data Collected
                </h4>
                <div className='flex flex-col gap-10'>
                  <h5 className='text-heading-6 desktop:text-heading-5 leading-tight font-medium font-poppins'>
                    Personal Data
                  </h5>
                  <p className=''>
                    While using our Service, we may ask you to provide us with
                    certain personally identifiable information that can be used
                    to contact or identify you ("Personal Data"). Personally
                    identifiable information may include, but is not limited to:
                  </p>
                  <ol className='flex flex-col list-decimal ml-10'>
                    <li className='list-item'> Email address</li>
                    <li className='list-item'>First name and last name</li>
                    <li className='list-item'>Cookies and Usage Data</li>
                  </ol>
                </div>
                <div className='flex flex-col gap-10'>
                  <h5 className='text-heading-6 desktop:text-heading-5 leading-tight font-medium font-poppins'>
                    Usage Data
                  </h5>
                  <p className=''>
                    We may also collect information how the Service is accessed
                    and used ("Usage Data"). This Usage Data may include
                    information such as your computer's Internet Protocol
                    address (e.g. IP address), browser type, browser version,
                    the pages of our Service that you visit, the time and date
                    of your visit, the time spent on those pages, unique device
                    identifiers and other diagnostic data.
                  </p>
                </div>
              </div>
            </div>
            <div className='flex flex-col gap-10'>
              <h3 className='text-heading-4 desktop:text-heading-3 leading-tight font-poppins font-medium my-10'>
                Tracking & Cookies Data
              </h3>
              <p className=''>
                We use cookies and similar tracking technologies to track the
                activity on our Service and hold certain information.
              </p>
              <p className=''>
                Cookies are files with small amount of data which may include an
                anonymous unique identifier. Cookies are sent to your browser
                from a website and stored on your device. Tracking technologies
                also used are beacons, tags, and scripts to collect and track
                information and to improve and analyze our Service
              </p>
              <p className=''>
                You can instruct your browser to refuse all cookies or to
                indicate when a cookie is being sent. However, if you do not
                accept cookies, you may not be able to use some portions of our
                Service.
              </p>
              <p className=''>Examples of Cookies we use:</p>
              <ol className='flex flex-col list-disc ml-10'>
                <li className='list-item'>
                  Session Cookies. We use Session Cookies to operate our
                  Service.
                </li>
                <li className='list-item'>
                  Preference Cookies. We use Preference Cookies to remember your
                  preferences and various settings.
                </li>
                <li className='list-item'>
                  Security Cookies. We use Security Cookies for security
                  purposes.
                </li>
              </ol>
              <div className='flex flex-col gap-10'>
                <h4 className='text-heading-4 leading-tight font-poppins font-medium my-10'>
                  Use of Data
                </h4>
                <p className=''>
                  Sonibble uses the collected data for various purposes:
                </p>
                <ol className='flex flex-col list-disc ml-10'>
                  <li className='list-item'>
                    To provide and maintain the Service
                  </li>
                  <li className='list-item'>
                    To notify you about changes to our Service
                  </li>
                  <li className='list-item'>
                    To allow you to participate in interactive features of our
                    Service when you choose to do so
                  </li>
                  <li className='list-item'>
                    To provide customer care and support
                  </li>
                  <li className='list-item'>
                    To provide analysis or valuable information so that we can
                    improve the Service
                  </li>
                  <li className='list-item'>
                    To monitor the usage of the Service
                  </li>
                  <li className='list-item'>
                    To detect, prevent and address technical issues
                  </li>
                </ol>
              </div>
              <div className='flex flex-col gap-10'>
                <h4 className='text-heading-4 leading-tight font-poppins font-medium my-10'>
                  Transfer Of Data
                </h4>
                <p className=''>
                  Your information, including Personal Data, may be transferred
                  to — and maintained on — computers located outside of your
                  state, province, country or other governmental jurisdiction
                  where the data protection laws may differ than those from your
                  jurisdiction.
                </p>
                <p className=''>
                  If you are located outside United States and choose to provide
                  information to us, please note that we transfer the data,
                  including Personal Data, to United States and process it
                  there.
                </p>
                <p className=''>
                  Your consent to this Privacy Policy followed by your
                  submission of such information represents your agreement to
                  that transfer.
                </p>
                <p className=''>
                  Sonibble will take all steps reasonably necessary to ensure
                  that your data is treated securely and in accordance with this
                  Privacy Policy and no transfer of your Personal Data will take
                  place to an organization or a country unless there are
                  adequate controls in place including the security of your data
                  and other personal information.
                </p>
              </div>
            </div>
            <div className='flex flex-col gap-10'>
              <h3 className='text-heading-4 desktop:text-heading-3 leading-tight font-poppins font-medium my-10'>
                Disclosure Of Data
              </h3>
              <div className='flex flex-col gap-10'>
                <h4 className='text-heading-4 leading-tight font-poppins font-medium'>
                  Use of Data
                </h4>
                <p className=''>
                  Sonibble may disclose your Personal Data in the good faith
                  belief that such action is necessary to:
                </p>
                <ol className='flex flex-col list-disc ml-10'>
                  <li className='list-item'>
                    To comply with a legal obligation
                  </li>
                  <li className='list-item'>
                    To protect and defend the rights or property of Sonibble
                  </li>
                  <li className='list-item'>
                    To protect the personal safety of users of the Service or
                    the public
                  </li>
                  <li className='list-item'>
                    To protect against legal liability
                  </li>
                </ol>
              </div>
            </div>
            <div className='flex flex-col gap-10'>
              <h3 className='text-heading-4 desktop:text-heading-3 leading-tight font-poppins font-medium my-10'>
                Security Of Data
              </h3>
              <p className=''>
                The security of your data is important to us, but remember that
                no method of transmission over the Internet, or method of
                electronic storage is 100% secure. While we strive to use
                commercially acceptable means to protect your Personal Data, we
                cannot guarantee its absolute security.
              </p>
            </div>
            <div className='flex flex-col gap-10'>
              <h3 className='text-heading-4 desktop:text-heading-3 leading-tight font-poppins font-medium my-10'>
                Service Providers
              </h3>
              <p>
                We may employ third party companies and individuals to
                facilitate our Service ("Service Providers"), to provide the
                Service on our behalf, to perform Service-related services or to
                assist us in analyzing how our Service is used.
              </p>
              <p className=''>
                These third parties have access to your Personal Data only to
                perform these tasks on our behalf and are obligated not to
                disclose or use it for any other purpose.
              </p>
              <div className='flex flex-col gap-10'>
                <h4 className='text-heading-4 leading-tight font-poppins font-medium'>
                  Analytics
                </h4>
                <p className=''>
                  We may use third-party Service Providers to monitor and
                  analyze the use of our Service.
                </p>
                <div className='flex flex-col gap-10'>
                  <h5 className='text-heading-6 desktop:text-heading-5 leading-tight font-poppins font-medium'>
                    Google Analytics
                  </h5>
                  <p className=''>
                    Google Analytics is a web analytics service offered by
                    Google that tracks and reports website traffic. Google uses
                    the data collected to track and monitor the use of our
                    Service. This data is shared with other Google services.
                    Google may use the collected data to contextualize and
                    personalize the ads of its own advertising network.
                  </p>
                  <p className=''>
                    You can opt-out of having made your activity on the Service
                    available to Google Analytics by installing the Google
                    Analytics opt-out browser add-on. The add-on prevents the
                    Google Analytics JavaScript (ga.js, analytics.js, and dc.js)
                    from sharing information with Google Analytics about visits
                    activity.
                  </p>
                  <p className=''>
                    For more information on the privacy practices of Google,
                    please visit the Google Privacy & Terms web page:
                  </p>
                  <p className=''>https://policies.google.com/privacy</p>
                </div>
              </div>
            </div>
            <div className='flex flex-col gap-10'>
              <h3 className='text-heading-4 desktop:text-heading-3 leading-tight font-poppins font-medium my-10'>
                Links To Other Sites
              </h3>
              <p>
                Our Service may contain links to other sites that are not
                operated by us. If you click on a third party link, you will be
                directed to that third party's site. We strongly advise you to
                review the Privacy Policy of every site you visit.
              </p>
              <p className=''>
                We have no control over and assume no responsibility for the
                content, privacy policies or practices of any third party sites
                or services.
              </p>
            </div>
            <div className='flex flex-col gap-10'>
              <h3 className='text-heading-4 desktop:text-heading-3 leading-tight font-poppins font-medium my-10'>
                Children's Privacy
              </h3>
              <p>
                Our Service does not address anyone under the age of 18
                ("Children").
              </p>
              <p className=''>
                We do not knowingly collect personally identifiable information
                from anyone under the age of 18. If you are a parent or guardian
                and you are aware that your Children has provided us with
                Personal Data, please contact us. If we become aware that we
                have collected Personal Data from children without verification
                of parental consent, we take steps to remove that information
                from our servers.
              </p>
            </div>
            <div className='flex flex-col gap-10'>
              <h3 className='text-heading-4 desktop:text-heading-3 leading-tight font-poppins font-medium my-10'>
                Changes To This Privacy Policy
              </h3>
              <p>
                We may update our Privacy Policy from time to time. We will
                notify you of any changes by posting the new Privacy Policy on
                this page.
              </p>
              <p className=''>
                We will let you know via email and/or a prominent notice on our
                Service, prior to the change becoming effective and update the
                "effective date" at the top of this Privacy Policy.
              </p>
              <p className=''>
                You are advised to review this Privacy Policy periodically for
                any changes. Changes to this Privacy Policy are effective when
                they are posted on this page.
              </p>
            </div>
            <div className='flex flex-col gap-10'>
              <h3 className='text-heading-4 desktop:text-heading-3 leading-tight font-poppins font-medium my-10'>
                Contact Us
              </h3>
              <p>
                If you have any questions about this Privacy Policy, please
                contact us by email to{' '}
                <a
                  href='mailto:creative.sonibble@gmail.com'
                  target='_blank'
                  rel='noreferrer'
                  data-cursor-size='100'
                  data-cursor-exclusion
                >
                  creative.sonibble@gmail.com
                </a>
              </p>
            </div>
          </div>
        </div>
      </main>
    </>
  )
}

PrivacyPage.layout = (page) => {
  return <GeneralLayout>{page}</GeneralLayout>
}

export default PrivacyPage
