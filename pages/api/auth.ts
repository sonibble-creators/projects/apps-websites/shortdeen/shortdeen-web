import { sessionOptions } from '@hooks/auth'
import { withIronSessionApiRoute } from 'iron-session/next'

declare module 'iron-session' {
  interface IronSessionData {
    user?: any
  }
}

/**
 * # authenticationRouteHandler
 *
 * handle the authentication including thea
 * signin and signout
 *
 * @param req data requested send
 * @param res response to send after finish
 */
export default withIronSessionApiRoute(
  async function authenticationRouteHandler(req, res) {
    // check the method that being used
    if (req.method != 'POST') {
      res
        .status(405)
        .json({ message: 'Opps, only post method are allowed', code: 405 })
    }

    const data = req.body
    const action = data.action

    if (action == 'signin') {
      delete data.action
      req.session.user = data
      await req.session.save()
      res.send({})
    } else if (action == 'check') {
      const user = req.session.user
      if (user) {
        res.send(user)
      } else {
        res.send({})
      }
    } else if (action == 'logout') {
      req.session.destroy()
      res.send({})
    }
  },
  sessionOptions
)
