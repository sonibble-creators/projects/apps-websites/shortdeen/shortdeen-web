import { AwesomeButton, FlatButton } from '@components/base/button'
import { Form } from '@components/base/form'
import { TextInput } from '@components/base/input'
import GeneralLayout from '@components/base/layout/general'
import { zodResolver } from '@hookform/resolvers/zod'
import { useAlert } from '@hooks/alert'
import { AwesomeError } from '@models/error'
import { ShortenLink } from '@models/link'
import linkService from '@services/link'
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import Head from 'next/head'
import { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { z } from 'zod'
import { NextPageWithlayout } from './_app'

const shortenFormSchema = z.object({
  link: z
    .string()
    .min(1, { message: 'Please define the link you wanna shorten' })
    .url({ message: 'Opps, please add the valid url' }),
  customName: z.string().trim(),
})

/**
 * # HomePage
 *
 * the home page to showing the user
 * about way to shorten the url
 *
 * @returns JSX.Element
 */
const HomePage: NextPageWithlayout = (): JSX.Element => {
  const shortenedUrl = useQuery<ShortenLink | undefined | null>(
    ['shorten-url'],
    () => {
      return null
    }
  )
  const [domain, setDomain] = useState<string>()

  const alert = useAlert()
  const queryClient = useQueryClient()

  const shortenForm = useForm({
    mode: 'onChange',
    resolver: zodResolver(shortenFormSchema),
  })

  const shortenUrl = useMutation(linkService.shortenUrl, {
    onMutate: (variables) => {
      // clear the current url
      queryClient.setQueryData(['shorten-url'], null)
    },
    onSuccess: (data, variables, context) => {
      alert.show({
        type: 'success',
        title: 'Url Shorten',
        description: 'Your awesome url already shortened',
      })

      // clear the form and update the data
      shortenForm.reset()
      queryClient.setQueryData(['shorten-url'], data)
    },
    onError: (error: AwesomeError, variables, context) => {
      alert.show({ type: 'error', title: 'Opps', description: error.message })

      // clear form and update data
      shortenForm.reset()
      queryClient.setQueryData(['shorten-url'], null)
    },
  })

  const onShortenFormSubmitted: (formData: any) => void = (formData) => {
    shortenUrl.mutate(formData)
  }

  const copyShortenUrl: () => void = async () => {
    const currentShortenUrl = `${domain}/${shortenedUrl.data?.shortLink ?? ''}`

    if ('clipboard' in navigator) {
      await navigator.clipboard.writeText(currentShortenUrl)
    }

    alert.show({
      type: 'success',
      title: 'Coppied',
      description: 'The Shorten link already coppied into your clipboard',
    })
  }

  useEffect(() => {
    // check the current domain and
    // can easyly show the current url
    setDomain(`${location.protocol}//${location.host}`)
  }, [])

  return (
    <>
      <Head>
        <title>Shorten your urls now - Shortdeen</title>
        <meta
          name='description'
          content='Stop wasting time searching for shortener url'
        />
      </Head>

      <main className='flex flex-col min-h-screen justify-center items-center'>
        <section className='container mx-auto flex flex-col'>
          <div className='flex tablet:w-8/12 desktop:w-8/12 mx-auto'>
            <h1
              className='text-heading-3 tablet:text-heading-2 desktop:text-display-1 font-medium font-poppins text-center leading-tight'
              data-aos='fade-up'
              data-cursor-exclusion
              data-cursor-size='180'
            >
              Everthing Become <span className='text-primary'>short.</span>
            </h1>
          </div>

          {/* shorten input */}
          <div
            className='flex flex-col mt-relaxed mx-auto w-full desktop:w-8/12 px-5 desktop:px-0'
            data-aos='fade-up'
            data-aos-delay='800'
          >
            <Form
              className=''
              id='shorten-form'
              context={shortenForm}
              onSubmit={onShortenFormSubmitted}
            >
              <TextInput
                name='link'
                placeholder='Type or paste your link'
                readonly={shortenUrl.isLoading}
              />
              <div className='flex flex-col tablet:flex-row justify-between items-center gap-6'>
                <TextInput
                  name='customName'
                  placeholder='Your custom link name'
                  className='w-10/12 tablet:w-6/12'
                  readonly={shortenUrl.isLoading}
                />

                <FlatButton disable={shortenUrl.isLoading} type='submit'>
                  {shortenUrl.isLoading ? (
                    <>
                      Shorting Url
                      <i className='fi fi-rr-spinner flex justify-center items-center text-[24px] animate-spin'></i>
                    </>
                  ) : (
                    <>
                      Shorten link
                      <i className='fi fi-rr-arrow-right flex justify-center items-center text-[24px]'></i>
                    </>
                  )}
                </FlatButton>
              </div>
            </Form>
          </div>

          <div className='flex w-8/12 mt-14 border-t border-t-gray-light dark:border-gray-darker dark:border-opacity-30 mx-auto'></div>

          {/* shorten results */}
          {shortenedUrl.data && (
            <div className={`flex flex-col mt-14 mx-auto w-8/12`}>
              <span className='font-medium text-gray-darker'>
                Your shorted link
              </span>
              <div className='flex items-center justify-between'>
                <span className='text-subtitle-large font-medium'>
                  {`${domain}/
                  ${shortenedUrl.data.shortLink}`}
                </span>

                <AwesomeButton onClick={copyShortenUrl}>
                  Copy Link
                  <i className='fi fi-rr-copy flex justify-center items-center text-[24px]'></i>
                </AwesomeButton>
              </div>
            </div>
          )}
        </section>
      </main>
    </>
  )
}

HomePage.layout = (page) => {
  return <GeneralLayout>{page}</GeneralLayout>
}

export default HomePage
