import linkService from '@services/link'
import { GetServerSideProps, NextPage } from 'next'

const getServerSideProps: GetServerSideProps = async ({ resolvedUrl }) => {
  const shortLink = resolvedUrl.replace('/', '')
  const sourceUrl = await linkService.loadSourceUrl({ shortLink })

  if (sourceUrl && sourceUrl != '') {
    return {
      redirect: {
        destination: sourceUrl,
        permanent: false,
      },
    }
  } else {
    return {
      notFound: true,
    }
  }
}

/**
 * # LinkPage
 *
 * page to handle the link redirection
 * to the original links
 *
 * @returns JSX.Element
 */
const LinkPage: NextPage = (): JSX.Element => {
  return <></>
}

export { getServerSideProps }
export default LinkPage
