import GeneralLayout from '@components/base/layout/general'
import Head from 'next/head'
import Image from 'next/image'
import { NextPageWithlayout } from './_app'

/**
 * # AboutPage
 *
 * Page to handle the contact info
 * showing some info about the contact
 *
 * @returns JSX.Element
 */
const AboutPage: NextPageWithlayout = (): JSX.Element => {
  return (
    <>
      <Head>
        <title>About - Shortdeen</title>
        <meta
          name='description'
          content='See what behind of the biggest idea'
        />
      </Head>

      <main className='flex flex-col py-64 text-body-normal tablet:text-body-large'>
        <section className='flex flex-col'>
          <div className='flex container mx-auto px-5 desktop:px-0'>
            <h1
              className='text-heading-3 desktop:text-heading-1 font-medium font-poppins leading-tight desktop:w-10/12'
              data-cursor-exclusion
              data-cursor-size='180'
              data-aos='fade-up'
            >
              Everthing of you will be shorten.
            </h1>
          </div>
          <div
            className='flex relative w-full mt-20 dekstop:mt-relaxed h-80 tablet:h-[40vh]  desktop:min-h-[850px] desktop:h-[calc(70vh)]'
            data-aos='fade-up'
            data-aos-delay='800'
          >
            <Image
              src={'/assets/image/about/shorten.png'}
              layout='fill'
              objectFit='cover'
              alt='Everthing of you will be shorten'
              priority
              data-cursor-color='#F6B3B7'
              data-cursor-size='60'
            />
          </div>
          <div className='flex flex-col mt-20 desktop:mt-relaxed items-center container mx-auto px-5 desktop:px-0'>
            <h3
              className='flex text-heading-4 desktop:text-heading-3 font-medium font-poppins desktop:w-10/12 text-center'
              data-cursor-exclusion
              data-cursor-size='180'
              data-aos='fade-up'
            >
              We provide you the best way to become make your link become short
            </h3>
            <div className='flex flex-col items-center desktop:flex-row w-full gap-10 tablet:gap-20 mt-20 desktop:mt-snug desktop:justify-center'>
              <div className='flex desktop:w-5/12'>
                <h4
                  className='text-heading-5 text-center desktop:text-left desktop:text-heading-4 font-medium font-poppins'
                  data-aos='fade-up'
                  data-aos-delay='200'
                >
                  What’s make the shorten different ?
                </h4>
              </div>
              <div className='flex desktop:w-6/12 flex-col gap-10 text-center desktop:text-left'>
                <span
                  data-aos='fade-up'
                  data-aos-delay='400'
                  data-cursor-exclusion
                  data-cursor-size='180'
                >
                  You have the way you can customize your link. We provide you
                  the best way to manage, monitoring, and see the static of the
                  link.
                </span>
                <span
                  data-aos='fade-up'
                  data-aos-delay='600'
                  data-cursor-exclusion
                  data-cursor-size='180'
                >
                  Just ensure your link become more professional and and easy to
                  use access through the way you can remember it.
                </span>
              </div>
            </div>
          </div>
        </section>
        <section className='flex flex-col container mx-auto mt-40 desktop:mt-extra px-5 desktop:px-0'>
          <div className='flex flex-col gap-10'>
            <h1
              className='text-heading-3 desktop:text-heading-1 font-medium font-poppins leading-tight desktop:w-8/12'
              data-aos='fade-up'
            >
              Share everywhere you want.
            </h1>
            <span
              className='desktop:w-8/12'
              data-aos='fade-up'
              data-aos-delay='200'
            >
              Evertime you shorten the url. We make them become more easy to
              capture and easy to remember the link. Now the way you can share
              and publish your link everwhere. Though Social media, website,
              shops.
            </span>
          </div>
          <div
            className='flex relative w-full mt-20 desktop:mt-relaxed h-80 tablet:h-[32vh] desktop:min-h-[700px] desktop:h-[calc(50vh)]'
            data-aos='fade-up'
            data-aos-delay='400'
          >
            <Image
              src={'/assets/image/about/share.png'}
              layout='fill'
              objectFit='cover'
              alt='Share everywhere you want'
              priority
              data-cursor-color='#8F7CFF'
              data-cursor-size='60'
            />
          </div>
        </section>
        <section className='flex flex-col mt-40 desktop:mt-extra'>
          <div className='flex flex-col gap-10 container mx-auto px-5 desktop:px-0'>
            <h1
              className='text-heading-3 desktop:text-heading-1 font-medium font-poppins leading-tight desktop:w-8/12'
              data-aos='fade-up'
            >
              Customize your loved link.
            </h1>
            <span
              className='desktop:w-8/12'
              data-aos='fade-up'
              data-aos-delay='200'
            >
              Create your account then start to customize your link name. Pick
              the best name of your link that will match about the context. We
              ensure you’re will be the next one of loved name
            </span>
          </div>
          <div
            className='flex relative w-full mt-20 desktop:mt-relaxed h-80 tablet:h-[40vh] desktop:min-h-[850px] desktop:h-[calc(70vh)]'
            data-aos='fade-up'
            data-aos-delay='400'
          >
            <Image
              src={'/assets/image/about/customize.png'}
              layout='fill'
              objectFit='cover'
              alt='Customize your loved link'
              priority
              data-cursor-color='#FA9BBB'
              data-cursor-size='60'
            />
          </div>
        </section>
      </main>
    </>
  )
}

AboutPage.layout = (page) => {
  return <GeneralLayout>{page}</GeneralLayout>
}

export default AboutPage
