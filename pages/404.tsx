import { FlatButton } from '@components/base/button'
import { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import { useRouter } from 'next/router'

/**
 * # NotFound
 *
 * not found page that showing some error and trouble when the
 * page not found or with status code `404`
 *
 * @returns JSX.Element
 */
const NotFound: NextPage = (): JSX.Element => {
  const router = useRouter()

  const createShortenLink: () => void = () => {
    router.push('/')
  }

  return (
    <>
      <Head>
        <title>Not Found - Shortdeen</title>
        <meta name='description' content='Opps, the page not found' />
      </Head>

      <main className='flex flex-col container px-5 desktop:px-0 m-auto py-10 desktop:py-64'>
        <div className='flex flex-col items-center'>
          <div className='flex relative w-full desktop:w-7/12 h-[240px] desktop:h-[400px]'>
            <Image
              src='/assets/image/empty.svg'
              layout='fill'
              objectFit='contain'
              alt='Not Found'
            />
          </div>
          <h3 className='text-heading-4 desktop:text-heading-3 font-medium font-poppins mt-16'>
            Opps, Not Found.
          </h3>
          <span className='text-body-normal desktop:text-body-large desktop:w-7/12 text-center mt-10 leading-relaxed'>
            Opps, someting happen, the page, link url you're looking for is not
            found. Please ensure the link is correct or if this is the link
            ensure the link still persist in the database. If not found please
            create a new one.
          </span>
        </div>
        <div className='flex justify-center mt-28'>
          <FlatButton onClick={createShortenLink} type='button'>
            Shorten link
            <i className='fi fi-rr-arrow-right flex justify-center items-center text-[24px]'></i>
          </FlatButton>
        </div>
      </main>
    </>
  )
}

export default NotFound
