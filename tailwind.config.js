const plugin = require('tailwindcss/plugin')
const tailwindForms = require('@tailwindcss/forms')

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    fontFamily: {
      poppins: ['Poppins', 'sans-serif'],
      sans: ['DM Sans', 'sans-serif'],
    },
    colors: {
      black: '#000000',
      white: '#FFFFFF',
      primary: '#79E845',
      gray: {
        lighter: '#F8F9FF',
        light: '#EFF2FC',
        normal: '#DDE1ED',
        dark: '#8F97B0',
        darker: '#697390',
      },
      red: '#E10606',
      green: '#0ED776',
      transparent: 'transparent',
    },
    screens: {
      tablet: '640px',
      desktop: '1024px',
      'large-desktop': '1980px',
    },
    fontSize: {
      'display-1': '5.375rem',
      'heading-1': '4rem',
      'heading-2': '3.25rem',
      'heading-3': '2.5rem',
      'heading-4': '1.75rem',
      'heading-5': '1.5rem',
      'heading-6': '1.25rem',
      'subtitle-large': '1.25rem',
      'subtitle-normal': '1.125rem',
      'body-large': '1.25rem',
      'body-normal': '1.0625rem',
      'body-small': '1rem',
      button: '1rem',
      labels: '.875rem',
    },
    animation: {
      skeleton: 'skeleton 2s cubic-bezier(0.4, 0, 0.6, 1) infinite',
    },
    keyframes: {
      skeleton: {
        '0%': { 'background-poisiton': '-468px 0', opacity: '0.8' },
        '50%': { opacity: '0.4' },
        '100%': { 'background-poisiton': '468px 0', opacity: '1.0' },
      },
    },
    extend: {
      lineHeight: {
        'extra-tight': '1',
      },
      margin: {
        relaxed: '140px',
        snug: '300px',
        extra: '400px',
      },
      gap: {
        relaxed: '140px',
        snug: '300px',
        extra: '400px',
      },
    },
  },
  plugins: [
    tailwindForms,
    plugin(function ({ addUtilities, matchUtilities, theme }) {
      addUtilities({
        '.no-scrollbar': {
          '-ms-overflow-style': 'none',
          'scrollbar-width': 'none',
        },
        '.no-scrollbar::-webkit-scrollbar': {
          display: 'none',
        },
      }),
        matchUtilities(
          {
            'text-stroke': (value) => ({
              '-webkit-text-stroke-width': `${value}px`,
            }),
          },
          { values: theme('strokeWidth') }
        )
      matchUtilities(
        {
          'text-stroke': (value) => ({
            '-webkit-text-stroke-color': value,
          }),
        },
        { values: theme('colors') }
      )
    }),
  ],
}
